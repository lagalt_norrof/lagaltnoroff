# LagaltNoroff.NET

## Installation

### Backend:
#### 1: Clone the project

#### 2: Change the ``"DefaultConnection": "CHANGE ME"`` to your own Database connection string in the ``appsettings.json`` file in the API

#### 3: Open the package manager console and run the command: ``update-database``
##### (Nuget packages should be installed already but if they are not then install the following nuget packages: AutoMapper, AutoMapper.Extensions.Microsoft.DependencyInjection, Microsoft.AspNetCore.Authentication.JwtBearer, Microsoft.EntityFrameworkCore, Microsoft.EntityFrameworkCore.Tools, Microsoft.EntityFrameworkCore.SqlServer, Newtonsoft.Json, System.Net.Http)

### That should set you up for the BackEnd, now onto the...

### Frontend:
#### 1: run ``npm-install`` in lagaltfrontend in the terminal in your IDE

#### 2: That's it! Now start the API from your IDE where you have your API solution open and run ``npm start`` in your IDE where you have the frontend solution open.

##### See the Docs folder in the repository to find out more about what this project was all about and check the ´´User Manual´´ to see how to use our application!


## Authors
Carl-Johan[@carljohan.maelan]

Samuel Kesete[@SamuelKesete]

Carl Jägerhill[@CarlJagerhill]


