import { useContext } from "react";
import { createContext, useState, setState } from "react";
import { API_URL } from "../constants/API";
import keycloak from "../keycloak";

const UserContext = createContext();

export function ContextData(){
    return useContext(UserContext);
}

export const UsersContext = ({children}) => {
    const [user, setUser] = useState([]);
    if(keycloak.authenticated){
        fetch(`${API_URL}/User/${keycloak.tokenParsed.sub}/GetUserById`)
        .then(results => results.json())
        .then(results => {
            localStorage.setItem('userId', results.id);
            setUser(results.id);
            console.log(results.id + results.name);
        });
    
        const value = {
            user,
        };
        return(
            <UserContext.Provider value = {value}>
            {children}
            </UserContext.Provider>
        )
    }
}