import {API_URL} from "../../constants/API";

export const fetchUser = () => {
    return fetch(`${API_URL}/User/GetAllUsers`)
    .then((response) => response.json())
    .then((response) => response.data);
}

export const fetchUserProjectById = (userId) => {
    const resp = fetch(`${API_URL}/User/${userId}/GetProjectsInUserById`)
    .then((response) => response.json())
    .then((response) => response.data);
    console.log(resp);
    return resp;
}

export const fetchUserPersonalProjectById = (userId) => {
    const resp = fetch(`${API_URL}/Portfolio/${userId}/getUsersPortfolios`)
    .then((response) => response.json())
    .then((response) => response.data)
    console.log(resp);
    return resp;
}


export const putUser = (user) => {
    return fetch(`${API_URL}/Portfolio/${user.userId}/updatePortfolio`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(user),
    })
    .then((response) => {
        console.log(response)
        if(!response.ok) {
            const {error} = response.json();
            throw Error(error);
        }
    });
};


export const PostUserPortfolio = (project, token) => {
    return fetch(`${API_URL}/Portfolio`, {
        method: "POST",
        headers: new Headers({
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        }),
        body: JSON.stringify(project),
    })
    .then(async (response) => {
        if(!response.ok) {
            const {error} = await response.json();
            throw Error(error);
        }
        return response.json();
    })
    .then((response) => response.data);
}