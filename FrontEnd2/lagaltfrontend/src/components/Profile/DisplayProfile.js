import axios from "axios";
import { Component, useContext, useEffect, useState } from "react";
import { API_URL } from "../../constants/API";
import keycloak from "../../keycloak";
import Spinner from 'react-bootstrap/Spinner';
import { Button, Modal, Form } from "react-bootstrap";



export const UserData = () => {
  const  [userData, setUserData] = useState([]);
  useEffect(() => {
    axios
    .get(`${API_URL}/User/${keycloak.tokenParsed.sub}/GetUserById`)
    .then((response)=> {
      const data = response.data;
      setUserData([...userData, ...data]);
    })
  }, [userData]);

  useEffect(() => {
    console.log("User:", userData);
  }, [userData]);
  return(
    <>
    {userData.map((user) => {
      <div>{user.name}</div>
    })}
    </>
  )
}

export const EditProfile = () => {
const [show, setShow] = useState(false);
const handleClose = () => setShow(false);
const handleShow = () => setShow(true);
const [imageURL, setImageUrl] = useState("");
const [Information, setInformation] = useState("");

const handleSubmit = (e) => {
  e.preventDefault();
  const name = keycloak.tokenParsed.preferred_username;
  const userId = keycloak.tokenParsed.sub;
  const hidden = false;
  const  id = localStorage.getItem('userId')
  const user = {id, name, imageURL, Information, hidden, userId};

  try {
      fetch(`${API_URL}/User/${id}`, {
          method: 'PATCH',
          mode: 'cors',
          headers: new Headers({
              "Content-Type": "application/json",
              Authorization: `Bearer ${keycloak.token}`,
          }),
          body: JSON.stringify(user),
      }).then(() => {
        console.log(user);
          console.log("updated user")
      })

  } catch (error) {
      console.log(error)
  }



}

  return (
    <div>
      <Button variant="primary" onClick={handleShow}>
        Edit Profile
      </Button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Profile</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3 w-60 p-3" controlId="formBasicName">
              <Form.Control
                type="EditProfile"
                placeholder="image"
                required
                value={imageURL}
                onChange={(e) => setImageUrl(e.target.value)}
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>
            <Form.Group
              className="mb-3 mb-3 w-60 p-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Control
                placeholder="Information"
                as="textarea"
                rows={3}
                required
                value={Information}
                onChange={(e) => setInformation(e.target.value)}
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

class DisplayProfile extends Component{
	
 	
	// Constructor
	constructor(props) {
		const items = props.items;
     const projects = props.projects;
		super(props);

		this.state = {
			items: [],
			DataisLoaded: false,
       projects: [],
		};


	}

	// ComponentDidMount is used to
	// execute the code
	componentDidMount() {
		fetch(`https://localhost:7104/api/User/${keycloak.tokenParsed.sub}/GetUserById`,{
            method: 'GET',
            mode: 'cors',
            headers: new Headers({
                'Access-Control-Allow-Origin':'*',
                "Content-Type": "application/json",
            })
        })
			.then((res) => res.json())
			.then((json) => {
				this.setState({
					items: json,
					DataisLoaded: true
				});
                console.log(json);
			});
      


       console.log(localStorage.getItem('userId')); 

	}
    

	render() {
		const { DataisLoaded, items, projects } = this.state;

		if (!DataisLoaded){
      return(

      <Spinner animation="border" role="status">
     <span className="visually-hidden">Loading...</span>
    </Spinner>
      )

    }       

		return (
      <>
      <EditProfile/>
        <div className="row row-cols-2">
          {
            <ol>
              <div className="col">
                <div className="profileCard">
                  <div>
                    <img
                      className="profilePicture"
                      src={`${items.imageURL}`}
                      alt="ProfilePicture"
                      height="250"
                      width="250"
                    ></img>
                  </div>
                  <div>
                    <h2>{items.name}</h2>
                  </div>
                  <div className="title">
                    <h3>About {items.name}:</h3>
                    {items.information}
                  </div>
                </div>
              </div>
            </ol>
          }
        </div>
      </>
    );
	}
}


export default DisplayProfile;