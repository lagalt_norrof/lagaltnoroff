import React from 'react';
import App from '../../App.css'
import { UsersContext } from '../../utils/UserContext';
import DisplayProfile from './DisplayProfile';
import DisplayProfileProjects from './DisplayProfileProjects';


const ProfileList = () => {
    return (
     
        <div className='row mx-md-n5 mt-5'>
            <UsersContext>
            <DisplayProfile/>
            <DisplayProfileProjects/>
            </UsersContext>
        </div>
       

    )
}

export default ProfileList;