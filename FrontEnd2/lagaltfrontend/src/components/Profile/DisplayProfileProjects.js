import { Component, useContext } from "react";
import { API_URL } from "../../constants/API";
import keycloak from "../../keycloak";
import { UserContext } from "../../utils/UserContext";
import App from "../../App.css";
import Spinner from "react-bootstrap/Spinner";

class DisplayProfileProjects extends Component {


  // Constructor

  constructor(props) {
    const projects = props.projects;
    super(props);

    this.state = {
      projects: [],
      DataisLoaded: false,
    };


  }

  // ComponentDidMount is used to
  // execute the code
  componentDidMount() {
    const userId = localStorage.getItem('userId');
    fetch(`${API_URL}/User/${userId}/GetUserWithInfoById`, {
      method: 'GET',
      mode: 'cors',
      headers: new Headers({
        'Access-Control-Allow-Origin': '*',
        "Content-Type": "application/json",
      })
    })
      .then((res) => res.json())
      .then((proj) => {
        this.setState({
          projects: proj,
          DataisLoaded: true
        });
        console.log(proj);
      });

  }

  render() {
    const { projects, DataisLoaded } = this.state;
    if (!DataisLoaded)
      return (


        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>

      );
    return (
      <div className="row row-cols-2">
        {projects.map((project, index) => {
          return (
            <div className="col">
              <div className="categorysProject">
                <div  key={index}>
                  <img className="displayImageProjectInProfile" src={`${project.imageUrl} `} alt="projectimg" />
                  <h2>{project.name}</h2>
                  <p>{project.description}</p>
                  {project.skills.map((skill, sIndex) => {
                    return (
                      <div key={sIndex}>
                        <h4>{skill.skillName}</h4>
                      </div>
                    );
                  })}

                </div>
              </div>
            </div>
          );
        })}
      </div>
    );
  }


}


export default DisplayProfileProjects;