import { API_URL } from "../../constants/API";

export const fetchUserById = (userId) => {
  return fetch(`${API_URL}/User/${userId}`)
    .then((r) => r.json())
    .then((r) => r.data)
    .then((user) => {
      if (!user) {
        throw new Error("Could not find user with id: " + userId);
      }
      return user;
    });
};

export const fetchUserByUserId = (userId) => {
  try {
    return fetch(`https://localhost:7104/api/User/${userId}/GetUserById`, {
      mode: 'cors',
      method: "POST",
      headers: new Headers({
          'Access-Control-Allow-Origin':'*',
         "Content-Type": "application/json",
      }),
  }).then((r) =>
      r
        .json()
        .then((r) => r.data)
        .then((user) => {
          console.log(user);
          if (!user) {
            console.log("Could not find user with id: " + userId);
          }
          return user;
        })
    );
    
  } catch (error) {
    console.log(error);
  }
};

export const postUser = (user, token) => {
 
        
            return fetch(`${API_URL}/User/CreateUser`, {
                mode: 'cors',
                method: "POST",
                headers: new Headers({
                    'Access-Control-Allow-Origin':'*',
                   "Content-Type": "application/json",
                   Authorization: `Bearer ${token}`,
                }),
                body: JSON.stringify(user),
            })
            .then(async(response) => {
                if(!response.ok) {
                    const {error} = await response.json();
                    throw new Error(error);
                }
                return response.json();
            })

}
