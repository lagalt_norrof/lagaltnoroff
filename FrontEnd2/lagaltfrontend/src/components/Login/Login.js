import { useKeycloak } from '@react-keycloak/web'
import React from 'react'
import { useEffect, useRef, useContext } from 'react';
import { Button } from 'react-bootstrap';

import keycloak from '../../keycloak';
import { postUser } from './LoginApi';



export function Login() {
  const didMount = useRef(false);
  let authenticated = keycloak.authenticated;

  useEffect(() => {
    if (didMount.current && keycloak.subject !== undefined) {
      setUser();
    }
    else didMount.current = true;
  }, [authenticated])

  const setUser = () => {
    keycloak.loadUserProfile().then((profile) => {
      let keycloakUser = {
        name: profile.username,
        userId: keycloak.subject,
      };
      postUser(keycloakUser, keycloak.token).then((result) => {
        console.log(result);
      })
    })
  }

  const logoutUser = () =>{
    keycloak.logout();
    localStorage.clear();
  }

  return (
    <div>
      {keycloak && !!keycloak.authenticated && (
        <Button

          onClick={() => logoutUser()}
        >
          Log out
        </Button>
      )}
      {keycloak && !keycloak.authenticated && (
        <Button

          onClick={() => keycloak.login()}
        >
          Log In
        </Button>
      )
      }
    </div>
  )
}