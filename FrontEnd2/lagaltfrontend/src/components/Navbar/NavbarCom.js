import React, { Component } from "react";
import { Navbar, Nav, Container, Form, Button } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";
import Home from '../../View/Home';
import Projects from '../../View/Projects';
import Profile from '../../View/Profile'
import { Login } from "../Login/Login";
import '../../App.css'
import ProjectDetail from '../Project/ProjectDetail'
import CommentOnProject from "../Project/CommentOnProject";
import { ContextData, UsersContext } from "../../utils/UserContext";
import keycloak from "../../keycloak";
import LoggedInProjects from "../../View/Projects";
import { PrivateRoute } from "../../utils/PrivateRoute";


export default class NavbarCom extends Component {
  render() {
    return (
      <Router>
        <div>
          <Navbar
            className="navbar navbar-expand-lg  shadow-lg p-3 mb-5 bg-secondary"
            expand="lg"
          >
            <Container fluid>
              <Navbar.Brand>
                <Nav className="navbar-nav m-auto">
                  <Nav.Link
                    className="nav-link text-white text-uppercase"
                    as={Link}
                    to={"/"}
                  >
                    Lagalt
                  </Nav.Link>
                </Nav>
              </Navbar.Brand>

              <Navbar.Toggle aria-controls="navbarScroll" />
              <Navbar.Collapse id="navbarScroll collapse navbar-collapse">
                  <Navbar.Collapse id="navbarScroll collapse navbar-collapse">
                    <Nav className="navbar-nav m-auto">
                      {keycloak.authenticated &&(
                      <Nav.Link
                        className="nav-link text-white text-uppercase me-2 ml-20px"
                        as={Link}
                        to={"/Profile"}
                      >
                        {" "}
                        Profile
                      </Nav.Link>
                      )}
                    </Nav>
                    <Nav className="d-flex"></Nav>
                  </Navbar.Collapse>
                  <Navbar.Collapse id="navbarScroll collapse navbar-collapse">
                    <Nav className="navbar-nav m-auto">
                      {keycloak.authenticated &&(
                      <Nav.Link
                        className="nav-link text-white text-uppercase me-2 ml-20px"
                        as={Link}
                        to={"/loggedInProjects"}
                      >
                        {" "}
                        Projects
                      </Nav.Link>
                      )}
                    </Nav>
                    <Nav className="d-flex"></Nav>
                  </Navbar.Collapse>

              </Navbar.Collapse>
              <Login />
            </Container>
          </Navbar>
        </div>
        <div>
          <Switch>
            <Route exact path="/" component={() => <Redirect to="/home" />} />
            <Route exact path="/home" component={Home} />
            <UsersContext>
            <Route path="/profile" component={Profile} />
            <Route path="/loggedInProjects" component={LoggedInProjects} />
            </UsersContext>
          </Switch>
        </div>
      </Router>
    );
  }
}
