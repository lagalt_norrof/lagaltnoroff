import React from 'react';
import App from '../../App.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Dropdown from 'react-bootstrap/Dropdown';
import Modal from 'react-bootstrap/Modal';
import { API_URL } from '../../constants/API';
import { useState } from 'react';
import CategoryDropdown from './CategoryDropdown'
import keycloak from '../../keycloak';
import { useKeycloak } from '@react-keycloak/web';




const CreateProject = () => {

    const { keycloak } = useKeycloak();
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [imageUrl, setImageUrl] = useState('');
    const [status, settSatus] = useState('');
    const [categoryID, setcategoryID] = useState('');
    const [userId, setUserId] = useState('');
    const [state, setState] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        const userId = localStorage.getItem('userId')
        const project = { name, imageUrl, status, categoryID, description, userId };
        console.log(project)
        try {
            fetch(`${API_URL}/Projects`, {
                method: 'POST',
                mode: 'cors',
                headers: new Headers({
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${keycloak.token}`,
                }),
                body: JSON.stringify(project),
            }).then(() => {
                console.log("new project added")
            })

        } catch (error) {
            console.log(error)
        }



    }

    // modal
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);




    return (


        <>

            <div className='createproject'>

                <Button variant="btn btn-primary" onClick={handleShow}>
                    Create New Project
                </Button>
            </div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Create New Project</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group className="mb-3 w-60 p-3" controlId="formBasicName">
                            <Form.Label>Project name</Form.Label>
                            <Form.Control type="Project" placeholder="Project Name"
                                required value={name} onChange={(e) => setName(e.target.value)} />
                            <Form.Text className="text-muted">
                            </Form.Text>
                        </Form.Group>
                        <Form.Group className="mb-3 w-60 p-3" controlId="formBasicName">
                            <Form.Label>image</Form.Label>
                            <Form.Control type="Project" placeholder="image"
                                required value={imageUrl} onChange={(e) => setImageUrl(e.target.value)} />
                            <Form.Text className="text-muted">
                            </Form.Text>
                        </Form.Group>
                        <Form.Group className="mb-3 w-60 p-3" controlId="formBasicName">
                            <Form.Label>Status</Form.Label>
                            <Form.Control type="Project" placeholder="status"
                                required value={status} onChange={(e) => settSatus(e.target.value)} />
                            <Form.Text className="text-muted">
                            </Form.Text>
                        </Form.Group>
                        <Form.Group>
                            
                            <span value={categoryID} onChange={(e) => setcategoryID(e.target.value)}>
                                <CategoryDropdown />
                            </span>
                        </Form.Group>
                        <Form.Group className="mb-3 mb-3 w-60 p-3" controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Description of the project</Form.Label>
                            <Form.Control as="textarea" rows={3} required value={description} onChange={(e) => setDescription(e.target.value)} />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default CreateProject