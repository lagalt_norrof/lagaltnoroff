import { useEffect, useState } from 'react'
import axios from 'axios'
import App from "../../App.css"
import CommentOnProject from './CommentOnProject';
import { Modal, Button } from 'react-bootstrap'
import Spinner from 'react-bootstrap/Spinner';





const ProjectDetailInfo = (ProjectId) => {
    const [post, setPost] = useState([]);
    const [isLoading, setIsLoading] = useState(true)


    useEffect(() => {
        axios.get(`https://localhost:7104/api/Projects/${this.state.projectId}/GetProjectByID`)
            .then(res => {
                console.log(" the response" + res)
                setPost(res.data)
                setIsLoading(false)
               console.log(this.state.projectId);
            })

            .catch(err => {
                console.log(err)
            })
    }, []);



    const [isShow, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const HideShowComment = (e) => {
        e.preventDefault();
        handleClose();
    };

    if (isLoading) {
        return(
            <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
           </Spinner>
        )
    }

    return (
        <>


            <div className='row m-2'>
                <div className='col-lg-4'>
                    <div className='card mb-4'>
                        <div>
                            <img className="displayImageProject" src={`${post.imageUrl}`} alt='Portrait' />
                        </div>
                        <h2>{post.name}</h2>
                        <div>
                            {post.description}
                        </div>
                        <h4>
                            {post.status}
                        </h4>

                        <div className="btn-join" role="toolbar" aria-label="Toolbar with button groups">
                            <button type="button" className="btn-left btn btn-primary ">
                                Join
                            </button>
                            <button onClick={handleShow}>comment
                            </button>
                            <Modal show={isShow}>
                                <CommentOnProject />
                                <Button className='btn close-btn' variant="danger" onClick={handleClose}>
                                    Close
                                </Button>
                            </Modal>

                        </div>
                    </div>
                </div>
                <div className='col-lg-4' id='messegeBx'>
                    <div className='card mb-4 d-none' >
                        <CommentOnProject />
                    </div>
                </div>
            </div>

        </>
    )
}

export default ProjectDetailInfo


