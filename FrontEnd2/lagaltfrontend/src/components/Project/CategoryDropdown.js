
import React, { Component } from "react";

class Categories extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: [],
        };
    }

    componentDidMount() {
        this.setState({
            categories: [
                { CategoryId: 1 , categoryName: "Web Development"},
                { CategoryId: 2, categoryName: "Music" }, 
                { CategoryId: 3 , categoryName: "Film"},
                { CategoryId: 4  ,categoryName:"Game Development" }

            ]

        });
    }
    render() {
        const { categories } = this.state;
        let categoryList = categories.length > 0
            && categories.map((item, i) => {
            return (
                <option key={ i} value={item.CategoryId}>{item.categoryName}</option>
            )
        }, this );

        return(
            <div>
                <select className="form-select form-select-lg mb-3">
                    <option>
                    Choose Category 
                    </option>
                    {categoryList}
                </select>
            </div>
        );

    }
}

export default Categories;