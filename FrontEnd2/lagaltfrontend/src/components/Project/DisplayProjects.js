import axios from 'axios'
import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import ProjectDetail from './ProjectDetail'
import App from '../../App.css'

const DisplayProjects = () => {
    const [projectId, setProjectId] = useState([]);
    const history = useHistory()
    const [loading, setLoading] = useState(false);
    const [posts, setPosts] = useState([]);
    const [searchTitle, setSearchTitle] = useState("");





    useEffect(() => {
        const loadPosts = async () => {
            setLoading(true);
            const response = await axios.get(
                "https://localhost:7104/api/Projects/GetAllProjects"
            );
            setPosts(response.data);
            setLoading(false);
        };

        loadPosts();
    }, []);


    return (

        <>
            <div className="row row-cols-2">
                <div className='searchbar-container'>
                    <input className='searchbar' type="text" placeholder="Search for Project..."
                        onChange={(e) => setSearchTitle(e.target.value)}
                    />
                </div>

                {loading ? (
                    <h4>Loading ...</h4>
                ) : (
                    posts
                        .filter((value) => {
                            if (searchTitle === 'string' ? searchTitle.toLowerCase() : '') {
                                return value;
                            } else if (

                                value.name.toLowerCase().includes(searchTitle.toLowerCase())
                            ) {
                                return value;
                            }
                        })
                        .map(posts => (
                            <ol key={posts.id} >
                                <div className="col">
                                    <div className="categorysProject">
                                        <div>
                                            <img className="displayImageProject" src={`${posts.imageUrl}`} alt='Portrait' />
                                        </div>
                                        <h2>
                                            {posts.name}
                                        </h2>
                                    </div>
                                </div>
                            </ol>

                        ))
                )}

            </div>
            <div>
                
            </div>
        </>


    )
}

export default DisplayProjects
