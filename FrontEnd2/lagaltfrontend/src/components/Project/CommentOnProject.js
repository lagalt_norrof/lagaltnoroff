import React, { useEffect, useState } from 'react';
import { API_URL } from '../../constants/API'
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import CategoryDropdown from '../Project/CategoryDropdown'
import { Modal } from 'react-bootstrap'


const CommentOnProject = (user, token) => {

    const [message, setMessage] = useState('');
    const [dateT, setDate] = useState('');
    const [projectId, setProjectId] = useState('');
    const [userId, setUserId] = useState('');



    const handleSubmit = (e) => {
        e.preventDefault();
        const project = { message, dateT, projectId, userId };
        console.log(project)
        fetch(`${API_URL}/Comment`, {
            method: 'POST',
            headers: { "Content-Type": "application/json" },
            Authorization: `Bearer ${token}`,
            body: JSON.stringify(project, user),
        }).then(() => {
            console.log("new comment added")
        })



    }


    return (
        <>

            <Form onSubmit={handleSubmit}>

                <Form.Group className="mb-3 mb-3 w-60 p-3" controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Description of the project</Form.Label>
                    <Form.Control as="textarea" rows={3} value={message} onChange={(e) => setMessage(e.target.value)} />
                </Form.Group>

                <Form.Group className="mb-3 w-60 p-3" controlId="formBasicName">
                    <Form.Label>date</Form.Label>
                    <Form.Control type="date"
                        value={dateT} onChange={(e) => setDate(e.target.value)} />
                    <Form.Text className="date">
                    </Form.Text>
                </Form.Group>


                <Form.Group>
                    <Form.Label>ChoosCategory</Form.Label>
                    <span value={projectId} onChange={(e) => setProjectId(e.target.value)}>
                        <CategoryDropdown />
                    </span>
                </Form.Group>


                {/* <Form.Group className="mb-3 w-60 p-3" controlId="formBasicName">
                    <Form.Label>ProjectId</Form.Label>
                    <Form.Control type="Project" placeholder="ProjectId"
                        value={projectId} onChange={(e) => setProjectId(e.target.value)} />
                    <Form.Text className="text-muted">
                    </Form.Text>
                </Form.Group> */}

                <Form.Group className="mb-3 w-60 p-3" controlId="formBasicName">
                    <Form.Label>UserID</Form.Label>
                    <Form.Control type="Project" placeholder="userId"
                        value={userId} onChange={(e) => setUserId(e.target.value)} />
                    <Form.Text className="text-muted">
                    </Form.Text>
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>




        </>
    )
}

export default CommentOnProject