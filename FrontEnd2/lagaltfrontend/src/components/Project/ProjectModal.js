
import React, { useState }  from 'react'
import { Button, Modal } from 'react-bootstrap'

export const ProjectModal = (projectDescription, projectName) => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
  
    return (
    <>
        <Modal show={show} onHide={handleShow}>
                        <Modal.Header closeButton>
                          <Modal.Title>{projectName}</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>Project Resources: {projectDescription} </Modal.Body>
                        <Modal.Footer>
                          <Button variant="secondary" onClick={handleClose}>
                            Close
                          </Button>
                          <Button variant="primary" onClick={handleClose}>
                            Join Project
                          </Button>
                        </Modal.Footer>
                      </Modal>
    </>
  )
}
