import { API_URL } from "../../constants/API";

export const fetchPostedProjectById = (data) => {
    return fetch(`${API_URL}/Projects/${data.projectId}/GetProjectByID`, {
        method: "POST",
        headers: new Headers({
            "Content-Type": "application/json",
            Authorization: `Bearer ${data.token}`,
        }),
        body: JSON.stringify({Id: data.Id}),
    })
    .then(async (response) => {
        if(!response.ok) {
            const {error} = await response.json();
            throw Error(error);
        }
        return response.json();
    })
    .then((response) => response.data);
}


export const fetchPostComment = (comment) => {
    console.log(JSON.stringify(comment));
    return fetch(`${API_URL}/Comment`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(comment),
    })
    .then((response) => response.json())
    .then((response) => response.data)
}


export const fetchApplicationWithId = (projectId) => {
    console.log("testing fetch fetchapplicationwithid");
    return fetch(`${API_URL}/Application/${projectId}/GetApplicationById`)
    .then((response) => response.json())
    .then((response) => response.data);
};

export const postApplication = (application) => {
    console.log(JSON.stringify(application));
    return fetch(`${API_URL}/Application`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(application),
    })
    .then((response) => response.json())
    .then((response) => response.data);
};

export const putApplication = (application) => {
    return fetch(`${API_URL}/Application`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(application),
    })
    .then((response) => response.json())
    .then((response) => response.data);
};

export const fetchCommentByProjectId = (projectId) => {
    return(`${API_URL}/Projects/${projectId}/GetCommentsInProject`)
    .then((response) => response.json())
    .then((response) => response.data);
}