import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';




import { initialize } from './keycloak';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <h1>Keycloak is loading...</h1>
)

initialize()
  .then(() => {
    root.render(
      <React.StrictMode>
        <App />
      </React.StrictMode>
    );
  })
  .catch(() => {
    root.render(
      <React.StrictMode>
        <h1>Keycloak failed to load.</h1>
      </React.StrictMode>
    );
  });
