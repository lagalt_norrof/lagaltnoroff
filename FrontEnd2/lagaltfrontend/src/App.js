import './App.css';
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavbarCom from './components/Navbar/NavbarCom'
import { BrowserRouter } from 'react-router-dom';
import "react-bootstrap-carousel/dist/react-bootstrap-carousel.css";
import { ReactKeycloakProvider } from '@react-keycloak/web';
import keycloak from './keycloak';




const App = () => {


  return (
    <ReactKeycloakProvider authClient={keycloak}>
      <BrowserRouter>
        <div className='App'>
          <NavbarCom />
        </div>
      </BrowserRouter>
    </ReactKeycloakProvider>


  )
}

export default App


