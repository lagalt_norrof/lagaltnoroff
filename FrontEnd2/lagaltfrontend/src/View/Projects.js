import 'react-bootstrap-buttons/dist/react-bootstrap-buttons.css';
import axios from 'axios'
import React, {useEffect, useState} from 'react'
import { useHistory } from 'react-router-dom'
import ProjectDetail from '../components/Project/ProjectDetail';
import App from '../App.css';
import { API_URL } from '../constants/API';
import keycloak from '../keycloak';
import CreateProject from '../components/Project/CreateProject';
import { Button, Modal } from 'react-bootstrap';
import { ProjectModal } from '../components/Project/ProjectModal';
import { render } from '@testing-library/react';



const LoggedInProjects = () => {
 
  //let clickProjectId = 1;


// function hideModal() {
//     this.setState({ activeModal: null })
// }

  
  const [projectId, setProjectId] = useState([]);
  const history = useHistory()
  const [loading, setLoading] = useState(false);
  const [posts, setPosts] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const userId = localStorage.getItem('userId');
  const status = "Accepted";

 const application = { projectId, userId, status };


  const joinProject = (projectId) => {
    const message = "";
    const status = "Accepted";
    const userId = localStorage.getItem('userId');
    const projectBody = {userId, projectId, message, status }
    fetch(`${API_URL}/Application`, {
      method: "POST",
      mode: 'cors',
      headers: new Headers({
        'Content-Type': 'application/json',
        authorization: 'Bearer ' + keycloak.token,
      }),
      body: JSON.stringify(projectBody),
    })
    console.log(projectBody);
  }

  useEffect(() => {
    const loadPosts = async () => {
      setLoading(true);
      const response = await axios.get(
        "https://localhost:7104/api/Projects/GetSkillsInProjectById"
      );
      setPosts(response.data);
      setLoading(false);
      console.log(response.data);
    };

    loadPosts();
  }, []);

  const clickHandler = () =>{
    joinProject(projectId);
  }


  return (

    <>
      <div className="row row-cols-2">
        <div className='searchbar-container'>
          <input className='searchbar' type="text" placeholder="Search for Project..."
            onChange={(e) => setSearchTitle(e.target.value)}
          />
          <div className='createproject'>
            <CreateProject/>
          </div>
        </div>

        {loading ? (
          <h4>Loading ...</h4>
        ) : (
          posts
            .filter((value) => {
              if (searchTitle === 'string' ? searchTitle.toLowerCase() : '') {
                return value;
              } else if (

                value.name.toLowerCase().includes(searchTitle.toLowerCase())
              ) {
                return value;
              }
            })
            .map((project, index) => {
              return (
                <div className="col">
                  <div className="categorysProject">
                    <div key={index}>
                      <img
                        className="displayImageInProjectInProfile"
                        src={`${project.imageUrl} `}
                        alt="projectimg"
                        height="100px"
                        width="100px"
                      ></img>
                      <h2>{project.name}</h2>
                      <p>{project.description}</p>
                      <Button
                        key={project.id}
                        variant="primary"
                        onClick={() => {
                          handleShow();
                          setProjectId(project.id);
                          ;
                        }}
                      >
                        Join Project
                      </Button>
                      <Modal show={show} onHide={handleClose}>
                        <Modal.Header>Join Project</Modal.Header>
                        <Modal.Body>
                          Are you sure you want to join this project?
                        </Modal.Body>
                        <Modal.Footer>
                          <Button variant="primary" onClick={() => {clickHandler(); handleClose();}}>
                            Yes
                          </Button>
                          <Button variant="seconday" onClick={handleClose}>
                            No
                          </Button>
                        </Modal.Footer>
                      </Modal>
                      {project.skills.map((skill, pIndex) => {
                        return (
                          <div
                            key={pIndex}
                            className="text-primary d-flex justify-content-center"
                          >
                            <h4>{skill.skillName}</h4>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              );
            })
            
        )}

      </div>
      <div>
        
      </div>
    </>


  )
}

export default LoggedInProjects

