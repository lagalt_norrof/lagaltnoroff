﻿using API.Enums;
using API.Models.Domain;

namespace API.Database.SeedData
{
    public class Seed
    {
        public static ICollection<User> SeedUser()
        {
            ICollection<User> users = new List<User>()
            {
                new User
                {
                    ID = 1, Name = "Carl", ImageURL = "Www.google.com/bilder", Hidden = false, Information = "Cool kille", UserId = "idididididididid"
                },

                new User
                {
                    ID = 2, Name = "Calle", ImageURL = "Www.google.com/bilder1", Hidden = false, Information = "Cool kille1337", UserId = "idididididididid!!!!"
                },

                new User
                {
                    ID = 3, Name = "Samuel", ImageURL = "Www.google.com/docker", Hidden = false, Information = "Docker kungen", UserId = "huehueuehu"
                }
            };
            return users;
        }
        public static ICollection<Skills> SeedSkills()
        {
            ICollection<Skills> skills = new List<Skills>()
        {
            new Skills
            {
                ID = 1, SkillName = "React.js"
            },
            new Skills
            {
                ID = 2, SkillName = "Angular"
            },
            new Skills
            {
                ID = 3, SkillName = ".NET"
            },
            new Skills
            {
                ID = 4, SkillName = "C#"
            },
            new Skills
            {
                ID = 5, SkillName = "Azure"
            }
        };
            return skills;
        }
        public static ICollection<Project> SeedProjects()
        {
            ICollection<Project> projects = new List<Project>()
            {
                new Project
                {
                    Id = 1,
                    Name = "Social Media Platform with react",
                    Description = "Creating an Socia Media Platform respecting your integrity using React",
                    ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/2300px-React-icon.svg.png",
                    Status = "On-Hold",
                    CategoryID = 1,
                    UserId = 2
                },

                new Project
                {
                    Id = 2,
                    Name = "E-Commerce Store with Angular",
                    Description = "Creating a Web Shop for Cheap clothing using Angular",
                    ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/1200px-Angular_full_color_logo.svg.png",
                    Status = "Done",
                    CategoryID = 1,
                    UserId = 4

                },

                new Project
                {
                    Id = 3,
                    Name = "Web Development with JS",
                    Description = "Creating a JavaScript Web App using no Frameworks!",
                    ImageUrl = "https://pluralsight2.imgix.net/paths/images/javascript-542e10ea6e.png",
                    Status = "Started",
                    CategoryID = 1,
                    UserId = 3
                },

                new Project
                {
                    Id = 4,
                    Name = "App Development",
                    Description = "Using Flutter to develop an App together, join to find out more!",
                    ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Android_Studio_Icon_3.6.svg/1900px-Android_Studio_Icon_3.6.svg.png",
                    Status = "On-Hold",
                    CategoryID = 1,
                    UserId = 1
                },
                new Project
                {
                    Id = 5,
                    Name = "Backend development with .NET",
                    Description = "Developing an API using .NET CORE",
                    ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Microsoft_.NET_logo.svg/2048px-Microsoft_.NET_logo.svg.png",
                    Status = "Done",
                    CategoryID = 1,
                    UserId = 4
                },
                new Project
                {
                    Id = 6,
                    Name = "Backend development with Node.JS",
                    Description = "Learning Node.js together!",
                    ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/1200px-Node.js_logo.svg.png",
                    Status = "Started",
                    CategoryID = 1,
                    UserId = 2
                },
                 new Project
                {
                    Id = 7,
                    Name = "Making House music Using FL",
                    Description = "Learning FL and creating sick beats!",
                    ImageUrl = "https://th.bing.com/th/id/OIP.EwPINFtRsOLXbnz6tD79IQHaD8?w=322&h=180&c=7&r=0&o=5&pid=1.7",
                    Status = "Started",
                    CategoryID = 2,
                    UserId = 2
                },
                  new Project
                {
                    Id = 8,
                    Name = "Making short Scetches on YouTube",
                    Description = "For actors who want to improve their resume",
                    ImageUrl = "https://pngimg.com/uploads/youtube/youtube_PNG102354.png",
                    Status = "Started",
                    CategoryID = 3,
                    UserId = 2
                },
                   new Project
                {
                    Id = 9,
                    Name = "Creating a Pinball Royale game for Android",
                    Description = "Using Android Studio and Flutter to create a wicked game!",
                    ImageUrl = "https://th.bing.com/th/id/OIP.zfG8FW1Z61TDDwJjGR2T-AHaIU?w=157&h=180&c=7&r=0&o=5&pid=1.7",
                    Status = "Started",
                    CategoryID = 4,
                    UserId = 4
                },
                       new Project
                {
                    Id = 10,
                    Name = "Using Unity to create a Super Smash Bros-esque Game",
                    Description = "Join this project if you want to learn Unity together and contribute to making an awesome game",
                    ImageUrl = "https://th.bing.com/th/id/OIP.mZP9gluTRGJls4D3Ri7XRAHaHa?pid=ImgDet&rs=1",
                    Status = "Started",
                    CategoryID = 4,
                    UserId = 4
                },


            };

            return projects;
        }

        public static ICollection<Category> SeedCategory()
        {
            ICollection<Category> categories = new List<Category>()
            {
                new Category
                {
                    ID = 1,
                    Name = "Web development",

                },

                  new Category
                {
                    ID = 2,
                    Name = "Music",
                },
                    new Category
                {
                    ID = 3,
                    Name = "Film",
                },
                         new Category
                {
                    ID = 4,
                    Name = "Game development",
                }

            };
            return categories;

        }

        public static ICollection<Comment> SeedComments()
        {

            ICollection<Comment> comments = new List<Comment>()
            {
                new Comment
                {
                    CommentId = 1,
                    Message = "Lol ur project sux",
                    Date = new DateTime(2021,08,23),
                    ProjectId = 1,
                    UserId = 1,
                }
            };
            return comments;
        }

        public static ICollection<History> SeedHistory()
        {
            ICollection<History> history = new List<History>()
            {
                new History
                {
                    Id = 1,
                    TypeOfHistory = HistoryType.ProjectClickedOn,
                    UserId = 1,
                    ProjectId = 1,
                },
                new History
                {
                    Id = 2,
                    TypeOfHistory = HistoryType.ProjectAppliedToProject,
                    UserId = 2,
                    ProjectId = 1

                }
            };

            return history;
        }

        public static ICollection<Application> SeedApplication()
        {
            ICollection<Application> application = new List<Application>()
            {
                new Application
                {
                    Id = 1,
                    ProjectId = 2,
                    UserId = 2,
                    Message = "Jag vill gå med i ditt projekt!!!!!",
                    Status = "Pending"

                },
                 new Application
                {
                    Id = 2,
                    ProjectId = 1,
                    UserId = 3,
                    Message = "Jag hatar ditt projekt!!!!!",
                    Status = "Approved"
                },
                new Application
                {
                    Id = 3,
                    ProjectId = 4,
                    UserId = 2,
                    Message = "inte ens snyggt!",
                    Status = "Declined"
                }



            };
            return application;
        }



        public static ICollection<Link> SeedLinks()
        {
            ICollection<Link> links = new List<Link>()
            {
                new Link
                {
                    Id = 1,
                    Name = "Testing Links",
                    URL = "https://www.github.com/Carljagerhill",
                    ProjectId = 1,
                },

                new Link
                {
                    Id = 2,
                    Name = "Testing Links second time",
                    URL = "https://www.github.com/Carljagerhill",
                    ProjectId = 2,
                },

                new Link
                {
                    Id = 3,
                    Name = "Testing Links third time",
                    URL = "https://www.github.com/Carljagerhill",
                    ProjectId = 3,
                },
                 new Link
                {
                    Id = 4,
                    Name = "Testing Links fourth time",
                    URL = "https://www.github.com/Carljagerhill",
                    ProjectId = 4,
                },

                new Link
                {
                    Id = 5,
                    Name = "Testing Links fifth time",
                    URL = "https://www.github.com/Carljagerhill",
                    ProjectId = 5,
                },

                new Link
                {
                    Id = 6,
                    Name = "Testing Links sixth time",
                    URL = "https://www.github.com/Carljagerhill",
                    ProjectId = 6,
                },


            };
            return links;
        }
    }
}
