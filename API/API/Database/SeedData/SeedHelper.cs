﻿using API.Models.Domain;

namespace API.Database.SeedData
{
    public class SeedHelper
    {

        public static ICollection<Portfolio> SeedPortfolio()
        {
            ICollection<Portfolio> portfolios = new List<Portfolio>()
            {
                new Portfolio
                {
                    Id = 1,
                    Name = "Hobby-Project",
                    Link = "https://github.com/Carljagerhill/Reactivities2.0",
                    Description = "React app teached through udemy",
                    UserId = 1,
                },

                new Portfolio
                {
                    Id = 2,
                    Name = "Music project",
                    Link = "https://www.youtube.com/watch?v=c0-hvjV2A5Y",
                    Description = "Creating a playlist for all my followers",
                    UserId = 2,
                },

                new Portfolio
                {
                    Id = 3,
                    Name = "Film Project",
                    Link = "https://www.youtube.com/watch?v=GD-hQ7quniI",
                    Description = "Trying to learn how to edit good",
                    UserId = 3,
                },

                new Portfolio
                {
                    Id = 4,
                    Name = "Music edits",
                    Link = "https://www.youtube.com/watch?v=ALZHF5UqnU4",
                    Description = "Final product with some sick editing",
                    UserId = 2,
                },

                   new Portfolio
                {
                    Id = 5,
                    Name = "Hobby-Project",
                    Link = "https://github.com/Carljagerhill/RipoffSpotify",
                    Description = "Using Spotify Api to learn more about frontend development",
                    UserId = 1,
                },
            };
            return portfolios;
        }
    }
}
