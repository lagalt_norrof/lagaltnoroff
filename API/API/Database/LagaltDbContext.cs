﻿using System;
using API.Database.SeedData;
using API.Enums;
using API.Models.Domain;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;


namespace API.Database

{

    /// <summary>
    /// Database table
    /// </summary>
    public class LagaltDbContext : DbContext
    {
        private readonly IConfiguration _configuration;
        public DbSet<User> Users { get; set; }
        public DbSet<Skills> Skills { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<History> History { get; set; }
        public DbSet<Comment> Comment { get; set; }
        public DbSet<Application> Application { get; set; }
        public DbSet<Portfolio> Portfolios { get; set; }
        public DbSet<Link> Links { get; set; }


        public LagaltDbContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Our connectionstring builder
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connString = _configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            SqlConnectionStringBuilder builder = new();
            builder.DataSource = connString;
            builder.InitialCatalog = "LagaltDB";
            builder.IntegratedSecurity = true;
            builder.Encrypt = false;
            optionsBuilder.UseSqlServer(builder.ConnectionString);
        }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Seeded data

            modelBuilder.Entity<User>().HasData(Seed.SeedUser());
            modelBuilder.Entity<Skills>().HasData(Seed.SeedSkills());
            modelBuilder.Entity<Project>().HasData(Seed.SeedProjects());
            modelBuilder.Entity<Category>().HasData(Seed.SeedCategory());
            modelBuilder.Entity<Comment>().HasData(Seed.SeedComments());
            modelBuilder.Entity<History>().HasData(Seed.SeedHistory());
            modelBuilder.Entity<History>().Property(h => h.TypeOfHistory).HasConversion(t => t.ToString(), t => (HistoryType)Enum.Parse(typeof(HistoryType), t));
            modelBuilder.Entity<Application>().HasData(Seed.SeedApplication());
            modelBuilder.Entity<Portfolio>().HasData(SeedHelper.SeedPortfolio());
            modelBuilder.Entity<Link>().HasData(Seed.SeedLinks());

            //Seeded Relationships


            modelBuilder.Entity<User>()
                .HasMany(C => C.Skills).WithMany(S => S.Users).UsingEntity<Dictionary<string, object>>(
                    "UserSkills",
                    r => r.HasOne<Skills>().WithMany().HasForeignKey("SkillsId"),
                    l => l.HasOne<User>().WithMany().HasForeignKey("UsersId"),
                    join =>
                    {
                        join.HasKey("SkillsId", "UsersId");
                        join.HasData(
                            new { SkillsId = 1, UsersId = 1 },
                            new { SkillsId = 2, UsersId = 2 },
                            new { SkillsId = 4, UsersId = 3 },
                            new { SkillsId = 4, UsersId = 1 },
                            new { SkillsId = 5, UsersId = 2 },
                            new { SkillsId = 3, UsersId = 3 },
                            new { SkillsId = 3, UsersId = 1 },
                            new { SkillsId = 3, UsersId = 2 }
                            );
                    });

            //Seeded Relationships

            modelBuilder.Entity<User>()
                .HasMany(C => C.Projects).WithMany(S => S.Users).UsingEntity<Dictionary<string, object>>(
                    "UserProjects",
                    r => r.HasOne<Project>().WithMany().HasForeignKey("ProjectId"),
                    l => l.HasOne<User>().WithMany().HasForeignKey("UserId"),
                  join =>
                 {
                     join.HasKey("UserId", "ProjectId");
                     join.HasData(
                         new { UserId = 2, ProjectId = 1 },
                         new { UserId = 2, ProjectId = 6 },
                         new { UserId = 2, ProjectId = 2 },
                         new { UserId = 3, ProjectId = 3 },
                         new { UserId = 1, ProjectId = 4 },
                         new { UserId = 3, ProjectId = 5 },
                         new { UserId = 3, ProjectId = 6 },
                         new { UserId = 1, ProjectId = 7 },
                         new { UserId = 3, ProjectId = 8 },
                         new { UserId = 1, ProjectId = 9 },
                         new { UserId = 2, ProjectId = 10 }
                         );
                 });

            //Seeded Relationships

            modelBuilder.Entity("ProjectSkills").HasData
                (new { ProjectsId = 1, SkillsID = 1 },
                new { ProjectsId = 2, SkillsID = 2 },
                new { ProjectsId = 3, SkillsID = 3 },
                new { ProjectsId = 4, SkillsID = 4 },
                new { ProjectsId = 5, SkillsID = 5 },
                new { ProjectsId = 6, SkillsID = 2 },
                 new { ProjectsId = 7, SkillsID = 5 },
                new { ProjectsId = 8, SkillsID = 1 },
                 new { ProjectsId = 9, SkillsID = 5 },
                new { ProjectsId = 10, SkillsID = 4 }
                );

        }
    }
}
