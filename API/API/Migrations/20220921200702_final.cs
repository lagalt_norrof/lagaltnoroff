﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace API.Migrations
{
    public partial class final : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Skills",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SkillName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skills", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ImageURL = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Information = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Hidden = table.Column<bool>(type: "bit", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ImageUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Status = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CategoryID = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Category_CategoryID",
                        column: x => x.CategoryID,
                        principalTable: "Category",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "History",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TypeOfHistory = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_History", x => x.Id);
                    table.ForeignKey(
                        name: "FK_History_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Portfolios",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Link = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Portfolios", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Portfolios_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserSkills",
                columns: table => new
                {
                    SkillsId = table.Column<int>(type: "int", nullable: false),
                    UsersId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSkills", x => new { x.SkillsId, x.UsersId });
                    table.ForeignKey(
                        name: "FK_UserSkills_Skills_SkillsId",
                        column: x => x.SkillsId,
                        principalTable: "Skills",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserSkills_Users_UsersId",
                        column: x => x.UsersId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Application",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Application", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Application_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Application_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comment",
                columns: table => new
                {
                    CommentId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comment", x => x.CommentId);
                    table.ForeignKey(
                        name: "FK_Comment_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Comment_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Links",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    URL = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Links", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Links_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectSkills",
                columns: table => new
                {
                    ProjectsId = table.Column<int>(type: "int", nullable: false),
                    SkillsID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectSkills", x => new { x.ProjectsId, x.SkillsID });
                    table.ForeignKey(
                        name: "FK_ProjectSkills_Projects_ProjectsId",
                        column: x => x.ProjectsId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProjectSkills_Skills_SkillsID",
                        column: x => x.SkillsID,
                        principalTable: "Skills",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserProjects",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProjects", x => new { x.UserId, x.ProjectId });
                    table.ForeignKey(
                        name: "FK_UserProjects_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserProjects_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Category",
                columns: new[] { "ID", "Name" },
                values: new object[,]
                {
                    { 1, "Web development" },
                    { 2, "Music" },
                    { 3, "Film" },
                    { 4, "Game development" }
                });

            migrationBuilder.InsertData(
                table: "Skills",
                columns: new[] { "ID", "SkillName" },
                values: new object[,]
                {
                    { 1, "React.js" },
                    { 2, "Angular" },
                    { 3, ".NET" },
                    { 4, "C#" },
                    { 5, "Azure" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "ID", "Hidden", "ImageURL", "Information", "Name", "UserId" },
                values: new object[,]
                {
                    { 1, false, "Www.google.com/bilder", "Cool kille", "Carl", "idididididididid" },
                    { 2, false, "Www.google.com/bilder1", "Cool kille1337", "Calle", "idididididididid!!!!" },
                    { 3, false, "Www.google.com/docker", "Docker kungen", "Samuel", "huehueuehu" }
                });

            migrationBuilder.InsertData(
                table: "History",
                columns: new[] { "Id", "ProjectId", "TypeOfHistory", "UserId" },
                values: new object[,]
                {
                    { 1, 1, "ProjectClickedOn", 1 },
                    { 2, 1, "ProjectAppliedToProject", 2 }
                });

            migrationBuilder.InsertData(
                table: "Portfolios",
                columns: new[] { "Id", "Description", "Link", "Name", "UserId" },
                values: new object[,]
                {
                    { 1, "React app teached through udemy", "https://github.com/Carljagerhill/Reactivities2.0", "Hobby-Project", 1 },
                    { 2, "Creating a playlist for all my followers", "https://www.youtube.com/watch?v=c0-hvjV2A5Y", "Music project", 2 },
                    { 3, "Trying to learn how to edit good", "https://www.youtube.com/watch?v=GD-hQ7quniI", "Film Project", 3 },
                    { 4, "Final product with some sick editing", "https://www.youtube.com/watch?v=ALZHF5UqnU4", "Music edits", 2 },
                    { 5, "Using Spotify Api to learn more about frontend development", "https://github.com/Carljagerhill/RipoffSpotify", "Hobby-Project", 1 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "CategoryID", "Description", "ImageUrl", "Name", "Status", "UserId" },
                values: new object[,]
                {
                    { 1, 1, "Creating an Socia Media Platform respecting your integrity using React", "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/2300px-React-icon.svg.png", "Social Media Platform with react", "On-Hold", 2 },
                    { 2, 1, "Creating a Web Shop for Cheap clothing using Angular", "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/1200px-Angular_full_color_logo.svg.png", "E-Commerce Store with Angular", "Done", 4 },
                    { 3, 1, "Creating a JavaScript Web App using no Frameworks!", "https://pluralsight2.imgix.net/paths/images/javascript-542e10ea6e.png", "Web Development with JS", "Started", 3 },
                    { 4, 1, "Using Flutter to develop an App together, join to find out more!", "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Android_Studio_Icon_3.6.svg/1900px-Android_Studio_Icon_3.6.svg.png", "App Development", "On-Hold", 1 },
                    { 5, 1, "Developing an API using .NET CORE", "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Microsoft_.NET_logo.svg/2048px-Microsoft_.NET_logo.svg.png", "Backend development with .NET", "Done", 4 },
                    { 6, 1, "Learning Node.js together!", "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/1200px-Node.js_logo.svg.png", "Backend development with Node.JS", "Started", 2 },
                    { 7, 2, "Learning FL and creating sick beats!", "https://th.bing.com/th/id/OIP.EwPINFtRsOLXbnz6tD79IQHaD8?w=322&h=180&c=7&r=0&o=5&pid=1.7", "Making House music Using FL", "Started", 2 },
                    { 8, 3, "For actors who want to improve their resume", "https://pngimg.com/uploads/youtube/youtube_PNG102354.png", "Making short Scetches on YouTube", "Started", 2 },
                    { 9, 4, "Using Android Studio and Flutter to create a wicked game!", "https://th.bing.com/th/id/OIP.zfG8FW1Z61TDDwJjGR2T-AHaIU?w=157&h=180&c=7&r=0&o=5&pid=1.7", "Creating a Pinball Royale game for Android", "Started", 4 },
                    { 10, 4, "Join this project if you want to learn Unity together and contribute to making an awesome game", "https://th.bing.com/th/id/OIP.mZP9gluTRGJls4D3Ri7XRAHaHa?pid=ImgDet&rs=1", "Using Unity to create a Super Smash Bros-esque Game", "Started", 4 }
                });

            migrationBuilder.InsertData(
                table: "UserSkills",
                columns: new[] { "SkillsId", "UsersId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 2 },
                    { 3, 1 },
                    { 3, 2 },
                    { 3, 3 },
                    { 4, 1 },
                    { 4, 3 },
                    { 5, 2 }
                });

            migrationBuilder.InsertData(
                table: "Application",
                columns: new[] { "Id", "Message", "ProjectId", "Status", "UserId" },
                values: new object[,]
                {
                    { 1, "Jag vill gå med i ditt projekt!!!!!", 2, "Pending", 2 },
                    { 2, "Jag hatar ditt projekt!!!!!", 1, "Approved", 3 },
                    { 3, "inte ens snyggt!", 4, "Declined", 2 }
                });

            migrationBuilder.InsertData(
                table: "Comment",
                columns: new[] { "CommentId", "Date", "Message", "ProjectId", "UserId" },
                values: new object[] { 1, new DateTime(2021, 8, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Lol ur project sux", 1, 1 });

            migrationBuilder.InsertData(
                table: "Links",
                columns: new[] { "Id", "Name", "ProjectId", "URL" },
                values: new object[,]
                {
                    { 1, "Testing Links", 1, "https://www.github.com/Carljagerhill" },
                    { 2, "Testing Links second time", 2, "https://www.github.com/Carljagerhill" },
                    { 3, "Testing Links third time", 3, "https://www.github.com/Carljagerhill" },
                    { 4, "Testing Links fourth time", 4, "https://www.github.com/Carljagerhill" },
                    { 5, "Testing Links fifth time", 5, "https://www.github.com/Carljagerhill" },
                    { 6, "Testing Links sixth time", 6, "https://www.github.com/Carljagerhill" }
                });

            migrationBuilder.InsertData(
                table: "ProjectSkills",
                columns: new[] { "ProjectsId", "SkillsID" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 2 },
                    { 3, 3 },
                    { 4, 4 },
                    { 5, 5 },
                    { 6, 2 },
                    { 7, 5 },
                    { 8, 1 },
                    { 9, 5 },
                    { 10, 4 }
                });

            migrationBuilder.InsertData(
                table: "UserProjects",
                columns: new[] { "ProjectId", "UserId" },
                values: new object[,]
                {
                    { 4, 1 },
                    { 7, 1 },
                    { 9, 1 },
                    { 1, 2 },
                    { 2, 2 },
                    { 6, 2 },
                    { 10, 2 },
                    { 3, 3 },
                    { 5, 3 },
                    { 6, 3 },
                    { 8, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Application_ProjectId",
                table: "Application",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Application_UserId",
                table: "Application",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_ProjectId",
                table: "Comment",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_UserId",
                table: "Comment",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_History_UserId",
                table: "History",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Links_ProjectId",
                table: "Links",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Portfolios_UserId",
                table: "Portfolios",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_CategoryID",
                table: "Projects",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectSkills_SkillsID",
                table: "ProjectSkills",
                column: "SkillsID");

            migrationBuilder.CreateIndex(
                name: "IX_UserProjects_ProjectId",
                table: "UserProjects",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_UserSkills_UsersId",
                table: "UserSkills",
                column: "UsersId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Application");

            migrationBuilder.DropTable(
                name: "Comment");

            migrationBuilder.DropTable(
                name: "History");

            migrationBuilder.DropTable(
                name: "Links");

            migrationBuilder.DropTable(
                name: "Portfolios");

            migrationBuilder.DropTable(
                name: "ProjectSkills");

            migrationBuilder.DropTable(
                name: "UserProjects");

            migrationBuilder.DropTable(
                name: "UserSkills");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Skills");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Category");
        }
    }
}
