﻿using API.Models.Domain;
using API.Models.DTOs.SkillDTOs;
using API.Repositories.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SkillsController : ControllerBase
    {
        private readonly ISkillsRepository _repository;
        public SkillsController(ISkillsRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Getting all the skills
        /// </summary>
        /// <returns>All the skills</returns>
        [HttpGet("GetAllSkills")]
        public async Task<IEnumerable<SkillsDTO>> GetAllSkills()
        {
            return await _repository.GetAllSkills();
        }

        /// <summary>
        /// Getting a specific skill by id 
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id"></param>
        /// <returns>the skill with matching id</returns>
        [HttpGet("{id}/GetSkillByID")]
        public async Task<ActionResult<SkillsDTO>> GetSkillByID(int id)
        {
            if (!_repository.SkillExists(id))
            {
                return BadRequest("Couldnt find a project with that Id...");
            }

            var skill = await _repository.GetSkillById(id);
            return Ok(skill);
        }

        /// <summary>
        /// Creating a new skill
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="skill">Skill object</param>
        /// <returns>a new skill</returns>
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<SkillCreateDTO>> CreateSkill(SkillCreateDTO skill)
        {
            var newSkill = await _repository.CreateSkill(skill);
            return CreatedAtAction("GetSkillByID", new { }, skill);
        }


        /// <summary>
        /// Deleting a certain skill
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">id of the skill</param>
        /// <returns>a removed skill</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteSkill(int id)
        {
            if (!_repository.SkillExists(id))
            {
                return BadRequest("Couldnt find a skill with that Id...");
            }
            await _repository.DeleteSkill(id);
            return NoContent();
        }


        /// <summary>
        ///  updating a existing skill
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="skillUpdateDTO">object of skill</param>
        /// <returns>updated skill </returns>
        [HttpPut]
        [Authorize]
        public async Task<ActionResult> UpdateSkill(SkillsUpdateDTO skillUpdateDTO)
        {
            await _repository.UpdateSkills(skillUpdateDTO);
            return NoContent();

        }
    }
}
