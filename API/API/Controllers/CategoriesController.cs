﻿using API.Models.DTOs.CategoryDTOs;
using API.Repositories.Abstract;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryRepository _repository;

        public CategoriesController( ICategoryRepository repository)
        {
            _repository = repository;

        }
        /// <summary>
        /// Gets all the categories
        /// </summary>
        /// <returns>a list of categories</returns>
        [Authorize]
        [HttpGet("GetAllCategories")]
        public async Task<IEnumerable<CategoryDTO>> GetCategories()
        {
            return await _repository.GetAllCategories();
        }

        /// <summary>
        /// Gets the category with matching id
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">category id</param>
        /// <returns>the category with matching id</returns>
        [HttpGet("{id}/GetCategoryByID")]
        public async Task<ActionResult<CategoryDTO>> GetCategoryByID(int id)
        {
            if (!_repository.CategoryExists(id))
            {
                return BadRequest($"Couldnt find a category with that id: {id}");
            }

            var category = await _repository.GetCategoryById(id);
            return Ok(category);
        }

        /// <summary>
        /// Creates a category
        /// </summary>
        /// <param name="dto">category object</param>
        /// <returns>creates a new category to db</returns>
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<CategoryCreateDTO>> CreateCategory(CategoryCreateDTO dto)
        {
            await _repository.CreateCategory(dto);
            return CreatedAtAction("GetCategoryByID", new { }, dto);
        }

        /// <summary>
        /// Deletes a category
        /// </summary>
        /// <response code="204">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">category id</param>
        /// <returns>A deleted category</returns>
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {

            if (!_repository.CategoryExists(id))
            {
                return BadRequest($"Couldnt find a category with that id: {id}");
            }

            await _repository.DeleteCategory(id);
            return NoContent();
        }

        /// <summary>
        /// Updating a category
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="categoryDTO">category object</param>
        /// <param name="id">category id</param>
        /// <returns>updating a category</returns>
        [Authorize]
        [HttpPut("{id}/updateCategory")]
        public async Task<ActionResult> UpdateCategory (CategoryUpdateDTO categoryDTO, int id)
        {
            if (!_repository.CategoryExists(id))
            {
                return BadRequest($"Couldnt find a category with that id: {id}");
            }

            await _repository.UpdateCategory(categoryDTO);
            return NoContent();
        }
    }
}
