﻿using API.Models.DTOs.CommentDTOs;
using API.Repositories.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentRepository _repository;

        public CommentController(ICommentRepository repository)
        {
            _repository = repository;
        }


        /// <summary>
        /// Creates a new comment to project
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="createComment">comment body</param>
        /// <returns>a new comment</returns>
        [Authorize]
        [HttpPost]
        [ActionName(nameof(CreateComment))]
        public async Task<ActionResult<CommentCreateDTO>> CreateComment(CommentCreateDTO createComment)
        {
            await _repository.CreateComment(createComment);
            return CreatedAtAction(nameof(CreateComment), new { }, createComment);
        }

        /// <summary>
        /// Deletes a comment from db
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">comment id</param>
        /// <returns>a removed comment in db</returns>
        [Authorize]
        [HttpDelete]
        public async Task<IActionResult> DeleteComment(int id)
        {
            await _repository.DeleteComment(id);
            return NoContent();
        }
    }
}
