﻿using API.Models.DTOs.ApplicationDTOs;
using API.Repositories.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationController : ControllerBase
    {
        private readonly IApplicationRepository _repository;


        public ApplicationController(IApplicationRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Getting all applications
        /// </summary>
        /// <returns>gets a list of applications</returns>
        [HttpGet("GetAllApplications")]
        public async Task<ActionResult<IEnumerable<ApplicationDTO>>> GetAllApplications()
        {
            var application = await _repository.GetAllApplications();
            return Ok(application);
        }

        /// <summary>
        /// Gets category by id
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">category id</param>
        /// <returns>returns a category based on id</returns>
        [HttpGet("{id}/GetApplicationById")]
        public async Task<ActionResult<ApplicationDTO>> GetApplicationById(int id)
        {

            if (!_repository.ApplicationExists(id))
            {
                return BadRequest($"Couldnt find a application with that id: {id}");
            }

            var application = await _repository.GetApplicationByID(id);
            return Ok(application);
        }

        /// <summary>
        /// Creating a new application
        /// </summary>
        /// <param name="dto">application object</param>
        /// <returns>a new application</returns>
        [HttpPost]
        [ActionName(nameof(CreateApplication))]
        public async Task<ActionResult<ApplicationCreateDTO>> CreateApplication(ApplicationCreateDTO dto)
        {
            var application = await _repository.CreateApplication(dto);
            return CreatedAtAction(nameof(CreateApplication), new { }, dto);
        }

        /// <summary>
        /// Updating application
        /// </summary>
        /// <param name="dto">application object</param>
        /// <returns>a updated application</returns>
        [HttpPut]
        public async Task<ActionResult> UpdateApplication(ApplicationUpdateDTO dto)
        {
            await _repository.UpdateApplication(dto);

            return Ok(dto);
        }

        /// <summary>
        /// Deletes a application
        /// </summary>
        /// <param name="id">application id</param>
        /// <returns>Deletes the id with matching id</returns>
        /// <response code="204">On Sucess</response>
        /// <response code="400">On Failure</response>
        [Authorize]
        [HttpDelete("{id}/DeleteApplication")]
        public async Task<ActionResult> DeleteApplication(int id)
        {
            if (!_repository.ApplicationExists(id))
            {
                return BadRequest($"Couldnt find a application with that id: {id}");
            }

            await _repository.DeleteApplication(id);
            return NoContent();
        }
    }
}
