﻿using API.Models.DTOs.HistoryDTOs;
using API.Repositories.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistoryController : ControllerBase
    {
        private readonly IHistoryRepository _repository;

        public HistoryController(IHistoryRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Creates a new history
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="createHistory">history object</param>
        /// <returns>creates a new link into db</returns>
        [HttpPost]
        [ActionName(nameof(CreateHistory))]
        public async Task<ActionResult<HistoryCreateDTO>> CreateHistory(HistoryCreateDTO createHistory)
        {
            await _repository.CreateHistory(createHistory);
            return CreatedAtAction(nameof(CreateHistory), new { }, createHistory);
        }

        /// <summary>
        /// Gets all the histories
        /// </summary>
        
        /// <returns>a list of histories</returns>
        [Authorize]
        [HttpGet("GetAllHistories")]
        public async Task<ActionResult<IEnumerable<HistoryDTO>>> GetAllHistories()
        {
            var histories = await _repository.GetAllHistories();
            return Ok(histories);
        }

        /// <summary>
        /// Gets history based on history id
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">id of history</param>
        /// <returns>history with matching id</returns>
        [HttpGet("{id}/GetHistoryById")]
        public async Task<ActionResult<HistoryDTO>> GetHistoryById(int id)
        {
            if (!_repository.HistoryExists(id))
            {
                return BadRequest($"Couldnt find a history with that id: {id}");
            }

            var history = await _repository.GetHistoryById(id);
            return Ok(history);
        }
    }
}
