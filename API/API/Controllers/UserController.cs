﻿using API.Database;
using API.Models.Domain;
using API.Models.DTOs.UserDTOs;
using API.Repositories.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _repository;

        public UserController(IUserRepository repository)
        {
            _repository = repository;
        }


        /// <summary>
        /// Getting all the users
        /// </summary>
        /// <response code="200">On Sucess, gets all user in list</response>
        /// <response code="400">On Failure, couldnt get users</response>
        /// <returns>Gets all the users</returns>
        [HttpGet("GetAllUsers")]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetUsers()
        {
            var users = await _repository.GetAllUsers();
            return Ok(users);
        }

        /// <summary>
        /// Getting userby keycloak Id
        /// </summary>
        /// <response code="200">On Sucess, gets the specific user!</response>
        /// <response code="400">On Failure, validation error or wrong id</response>
        /// <param name="userId">Keycloak Id</param>
        /// <returns>User by Keycloak id</returns>
        [HttpGet("{userId}/GetUserById")]
        public async Task<ActionResult<UserDTO>> GetUserByID(string userId)
        {
            if(!_repository.userExistsString(userId))
            {
                return BadRequest("Couldnt find a user with that Id...");
            }


            var user = await _repository.GetUserByID(userId);
            return Ok(user);
        }

        /// <summary>
        /// Getting a user with information
        /// </summary>
        /// <response code="200">On Sucess, gets the specific user data</response>
        /// <response code="400">On Failure, validation error or wrong id!</response>
        /// <param name="id">id of the user</param>
        /// <returns> a user with information</returns>
        [HttpGet("{id}/GetUserWithInfoById")]
        public async Task<ActionResult<IEnumerable<UserProjectSkillsDTO>>> GetUserWithInfoById(int id)
        {
            if (!_repository.userExists(id))
            {
                return BadRequest("Couldnt find a user with that Id...");
            }

            var pskills = await _repository.GetUserWithInfoByID(id);
            return Ok(pskills);
        }


        /// <summary>
        /// Creating a new user
        /// </summary>
        /// <response code="200">On Sucess, Creates a new user to DB!</response>
        /// <response code="400">On Failure, validation error!</response>
        /// <param name="user">user object</param>
        /// <returns>a new user into db</returns>
        [Authorize]
        [HttpPost("CreateUser")]
        public async Task<ActionResult<UserCreateDTO>> CreateUser(UserCreateDTO user)
        {
            var newUser = await _repository.CreateUser(user);
            return CreatedAtAction("GetUserById", new {userId = user.UserId}, user);
        }

        /// <summary>
        /// Deleting a user by id
        /// </summary>
        /// <response code="204">On Sucess, Deletes the user from DB</response>
        /// <response code="400">On Failure, wrong id!</response>
        /// <param name="userId">users id</param>
        /// <returns>Deleted user in db</returns>
        [Authorize]
        [HttpDelete("DeleteUser/{userId}")]
        public async Task<ActionResult> DeleteUser(int userId)
        {
            if (!_repository.userExists(userId))
            {
                return BadRequest("Couldnt find a user with that Id...");
            }
            await _repository.DeleteUser(userId);
            return NoContent();
        }

        /// <summary>
        /// Updating a user with new data
        /// </summary>
        /// <response code="200">On Sucess, updated user stored in db</response>
        /// <response code="400">On Failure, validation error or wrong id</response>
        /// <param name="userUpdate">user object</param>
        /// <param name="id">id of the the targetted user</param>
        /// <returns> updated user</returns>
        [HttpPatch("{id}")]
        public async Task<ActionResult> UpdateUser(UserUpdateDTO userUpdate, int id)
        {
            if (!_repository.userExists(id))
            {
                return BadRequest("Couldnt find a user with that Id...");
            }

            await _repository.UpdateUser(userUpdate);
            return NoContent();
        }

        /// <summary>
        /// Getting projects that user are involved in
        /// </summary>
        /// <response code="200">On Sucess, gets all users inside of the list</response>
        /// <response code="400">On Failure, wrong id!</response>
        /// <param name="userId">targetted users id</param>
        /// <returns>projects of the user</returns>
        [HttpGet("{userId}/GetProjectsInUserById")]
        public async Task<ActionResult<IEnumerable<UserProjectDTO>>> GetProjectsInUserById(int userId)
        {
            if (!_repository.userExists(userId))
            {
                return BadRequest("Couldnt find a user with that Id...");
            }

            var result = await _repository.GetProjectsInUserById(userId);
            return Ok(result);
        }

        /// <summary>
        /// Gets the skills of the user
        /// </summary>
        /// <response code="200">On Sucess, gets the skills from the user</response>
        /// <response code="400">On Failure, wrong id</response>
        /// <param name="userId">users id</param>
        /// <returns>skills of the user</returns>
        [HttpGet("{userId}/GetSkillsInUserById")]
        public async Task<ActionResult<IEnumerable<UserSkillsDTO>>> GetSkillsInUserById(int userId)
        {

            if (!_repository.userExists(userId))
            {
                return BadRequest("Couldnt find a user with that Id...");
            }

            var result = await _repository.GetSkillsInUserById(userId);
            return Ok(result);
        }

        /// <summary>
        /// updating the skills of an user
        /// </summary>
        /// <response code="200">On Sucess, updated skills in DB</response>
        /// <response code="400">On Failure, unable to update due to validation</response>
        /// <param name="userId">users id</param>
        /// <param name="skillsId">skill id</param>
        /// <returns>updated skills in user</returns>

        [HttpPut("{userId}/UpdateSkillsInUser")]
        [Authorize]
        public async Task<ActionResult> UpdateSkillsInUser(int userId, int[] skillsId)
        {
            if (!_repository.userExists(userId))
            {
                return BadRequest("Couldnt find a user with that Id...");
            }

            await _repository.UpdateSkillsInUser(userId, skillsId);
            return NoContent();
        }
    }
}
