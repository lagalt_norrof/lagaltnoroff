﻿using API.Models.DTOs.PortfolioDTOs;
using API.Repositories.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PortfolioController : ControllerBase
    {
        private readonly IPortfolioRepository _repository;

        public PortfolioController(IPortfolioRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets all the portfolio existing
        /// </summary>
        /// <returns>return a list of portfolios</returns>
        [HttpGet("GetAllPortfolios")]
        public async Task<ActionResult<IEnumerable<PortfolioDTO>>> GetAllPortfolios()
        {
            var portfolios = await _repository.GetAllPortfolios();
            return Ok(portfolios);
        }


        /// <summary>
        /// Getting a users portfolio
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">id of user</param>
        /// <returns>users portfolios</returns>
        [HttpGet("{id}/getUsersPortfolios")]
        public async Task<ActionResult<IEnumerable<PortfolioDTO>>> GetUsersPortfolios(int id)
        {
            if (!_repository.PortfolioExists(id))
            {
                return BadRequest($"Couldnt find a portfolio with that id {id}");
            }

            var result = await _repository.GetUserPortfolio(id);
            return Ok(result);
        }

        /// <summary>
        ///  creating a new portfolio
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="portfolioDto">portfolio object</param>
        /// <returns>a created portfolio</returns>
        /// 
        [Authorize]
        [HttpPost]
        [ActionName(nameof(CreatePortfolio))]
        public async Task<ActionResult<PortfolioCreateDTO>> CreatePortfolio(PortfolioCreateDTO portfolioDto)
        {
            var portfolio = await _repository.CreatePortfolio(portfolioDto);
            return CreatedAtAction(nameof(CreatePortfolio), new { }, portfolio);
        }

        /// <summary>
        /// Deleting a portfolio
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">portfolio id</param>
        /// <returns>Deleted portfolio</returns>
        [Authorize]
        [HttpDelete("{id}/DeletePortfolio")]
        public async Task<ActionResult> DeletePortfolio(int id)
        {

            if (!_repository.PortfolioExists(id))
            {
                return BadRequest($"Couldnt find a portfolio with that id {id}");
            }

            await _repository.DeletePortfolio(id);
            return NoContent();
        }

        /// <summary>
        /// Updating a portfolio
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">id of portfolio</param>
        /// <param name="portfolioDTO">portfolio object</param>
        /// <returns>a updated portfolio</returns>
        [Authorize]
        [HttpPut("{id}/updatePortfolio")]
        public async Task<ActionResult> UpdatePortfolio(int id, PortfolioDTO portfolioDTO)
        {

            if (!_repository.PortfolioExists(id))
            {
                return BadRequest($"Couldnt find a portfolio with that id {id}");
            }

            await _repository.UpdatePortfolio(portfolioDTO);

            return NoContent();


        }

    }
}
