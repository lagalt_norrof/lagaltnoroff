﻿using API.Models.DTOs.ProjectDTOs;
using API.Repositories.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectRepository _repository;
        public ProjectsController(IProjectRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets all the project in list
        /// </summary>
        /// <returns>A list of all projects</returns>
        [HttpGet("GetAllProjects")]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> GetAllProjects()
        {
            var projects = await _repository.GetAllProjects();
            return Ok(projects);
        }

        /// <summary>
        /// Getting a specific project by id
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">id of the project</param>
        /// <returns>Gets the specific project with matching id</returns>
        [HttpGet("{id}/GetProjectByID")]
        public async Task<ActionResult<ProjectDTO>> GetProjectByID(int id)
        {
            if (!_repository.ProjectExists(id))
            {
                return BadRequest("Couldnt find a project with that Id...");
            }

            var project = await _repository.GetProjectByID(id);
            return Ok(project);
        }

        /// <summary>
        /// Gets project by matching input letters
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="input">user input</param>
        /// <returns>matching project by string</returns>
        [HttpGet("{input}/GetProjectByString")]
        public async Task<ActionResult<ProjectSearchDTO>> GetProjectByString(string input)
        {
            var project = await _repository.GetProjectByString(input);
            return Ok(project);
        }

        /// <summary>
        /// Creates a new project
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="dto">Project object</param>
        /// <returns>creating a new project into db</returns>
        [Authorize]
        [HttpPost]
        [ActionName(nameof(CreateProject))]
        public async Task<ActionResult<ProjectCreateDTO>> CreateProject(ProjectCreateDTO dto)
        {
            var project = await _repository.CreateProject(dto);
            return CreatedAtAction(nameof(CreateProject), new { }, dto);
        }

        /// <summary>
        /// Deleting a project
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">id of the projecft</param>
        /// <returns>Deleted project</returns>
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteProject(int id)
        {
            if (!_repository.ProjectExists(id))
            {
                return BadRequest("Couldnt find a project with that Id...");
            }

            await _repository.DeleteProject(id);
            return NoContent();
        }

        /// <summary>
        /// Updating a project
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="projectUpdateDTO">project object</param>
        /// <returns>a updated object</returns>
        [Authorize]
        [HttpPut]
        public async Task<ActionResult> UpdateProject(ProjectUpdateDTO projectUpdateDTO)
        {
            await _repository.UpdateProject(projectUpdateDTO);

            return NoContent();

        }

        /// <summary>
        /// All skills inside a project by id
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <returns>returning skills inside project</returns>
        [HttpGet("GetSkillsInProjectByID")]
        public async Task<ActionResult<IEnumerable<ProjectSkillsDTO>>> GetSkillsInProjectByID()
        {
            var result = await _repository.GetSkillsInProjectByID();
            return Ok(result);

        }

        /// <summary>
        /// updating skill inside of project
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="projectId">project id</param>
        /// <param name="skillsId">skill id</param>
        /// <returns>updated skills inside project</returns>
        [Authorize]
        [HttpPut("{projectId}/UpdateSkillInProject")]
        public async Task<ActionResult> UpdateSkillsInProject(int projectId, int[] skillsId)
        {
            if (!_repository.ProjectExists(projectId))
            {
                return BadRequest("Couldnt find a project with that Id...");
            }

            await _repository.UpdateSkillsInProject(projectId, skillsId);
            return NoContent();
        }

        /// <summary>
        /// Getting all users inside of a project
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">project id</param>
        /// <returns>all users inside the choosen project</returns>
        [HttpGet("{id}/GetUsersInProject")]
        public async Task<ActionResult<ProjectUserDTO>> GetUsersInProject(int id)
        {

            if (!_repository.ProjectExists(id))
            {
                return BadRequest("Couldnt find a project with that Id...");
            }

            var result = await _repository.GetUsersInProjectById(id);
            return Ok(result);
        }

        /// <summary>
        /// Updating users inside of a project
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="projectId">project id</param>
        /// <param name="userId">user id</param>
        /// <returns>updating list of users inside of project</returns>

        [Authorize]
        [HttpPut("{projectId}/UpdateUsersInProject")]
        public async Task<ActionResult> UpdateUsersInProject(int projectId, int[] userId)
        {
            if (!_repository.ProjectExists(projectId))
            {
                return BadRequest("Couldnt find a project with that Id...");
            }

            await _repository.UpdateUsersInProject(projectId, userId);
            return NoContent();
        }

        /// <summary>
        /// gets all comment posted on project
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">id of project</param>
        /// <returns>comments posted on the choosen project</returns>
        [HttpGet("{id}/GetCommentsInProject")]
        public async Task<ActionResult<ProjectCommentDTO>> GetCommentInProject(int id)
        {
            if (!_repository.ProjectExists(id))
            {
                return BadRequest("Couldnt find a project with that Id...");
            }

            var result = await _repository.GetCommentInProjectByID(id);
            return Ok(result);
        }
    }
}
