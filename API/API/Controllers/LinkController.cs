﻿using API.Models.DTOs.LinkDTOs;
using API.Repositories.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinkController : ControllerBase
    {
        private readonly ILinksRepository _repository;

        public LinkController(ILinksRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets all the links
        /// </summary>
        /// <returns>a list of links</returns>
        [HttpGet("GetAllLinks")]
        public async Task<ActionResult<IEnumerable<LinkDTO>>> GetAllLinks()
        {
            var links = await _repository.GetAllLinks();
            return Ok(links);
        }

        /// <summary>
        /// Getting a specific link
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">link id</param>
        /// <returns>a link based on id</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<LinkDTO>> GetLinkById(int id)
        {
            if (!_repository.LinkExists(id))
            {
                return BadRequest($"Couldnt find a link with that id: {id}");
            }

            var links = await _repository.GetLinksById(id);
            return Ok(links);
        }

        /// <summary>
        /// Creating a new link
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="dto">link object</param>
        /// <returns>a created link</returns>
        [Authorize]
        [HttpPost]
        [ActionName(nameof(CreateLink))]
        public async Task<ActionResult<LinkCreateDTO>> CreateLink(LinkCreateDTO dto)
        {
            var links = await _repository.CreateLink(dto);
            return CreatedAtAction(nameof(CreateLink), new { }, dto);
        }

        /// <summary>
        /// Deleting a link based on id
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="id">id on link</param>
        /// <returns>a deleted link</returns>
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteLink(int id)
        {
            if (!_repository.LinkExists(id))
            {
                return BadRequest($"Couldnt find a link with that id: {id}");
            }

            await _repository.DeleteLink(id);
            return NoContent();
        }

        /// <summary>
        /// Updating a link based on id
        /// </summary>
        /// <response code="200">On Sucess</response>
        /// <response code="400">On Failure</response>
        /// <param name="linkUpdateDTO">link object</param>
        /// <param name="id">id of the link</param>
        /// <returns>updated link</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateLink(LinkUpdateDTO linkUpdateDTO, int id)
        {
            if (!_repository.LinkExists(id))
            {
                return BadRequest($"Couldnt find a link with that id: {id}");
            }

            await _repository.UpdateLink(linkUpdateDTO);
            return NoContent();

        }
    }
}
