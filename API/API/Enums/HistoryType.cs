﻿namespace API.Enums
{
    /// <summary>
    /// HistoryType enum
    /// </summary>
    public enum HistoryType
    {
        ProjectClickedOn,
        ProjectAppliedToProject,
        ProjectSeenFromLandingPage,
    }
}
