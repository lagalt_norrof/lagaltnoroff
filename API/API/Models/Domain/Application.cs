﻿namespace API.Models.Domain
{
    public class Application
    {

        /// <summary>
        /// Model for application
        /// </summary>
        public int Id { get; set; }
        public string? Message { get; set; }
        public string Status { get; set; }


        /// <summary>
        /// Relationships
        /// </summary>
        public int ProjectId { get; set; }
        public Project Project { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }


    }
}
