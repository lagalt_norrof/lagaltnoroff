﻿namespace API.Models.Domain
{
    public class Skills
    {
        /// <summary>
        /// Model for skills
        /// </summary>
        public int ID { get; set; }
        public string SkillName { get; set; }

        /// <summary>
        /// Relationships
        /// </summary>
        public ICollection<User> Users { get; set; }
        public ICollection<Project> Projects { get; set; }
    }
}
