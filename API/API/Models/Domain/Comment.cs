﻿namespace API.Models.Domain
{
    public class Comment
    {

        /// <summary>
        /// Model for comment
        /// </summary>
        public int CommentId { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }

        /// <summary>
        /// Relationships
        /// </summary>
        public int UserId { get; set; }
        public int ProjectId { get; set; }
        public User User { get; set; }
        public Project Project { get; set; }

    }
}
