﻿namespace API.Models.Domain
{
    public class Portfolio
    {
        /// <summary>
        /// Model for portfolio
        /// </summary>
        public int Id { get; set; }

        public string Name { get; set; }

        public string Link { get; set; }

        public string Description { get; set; }


        /// <summary>
        /// Relationships
        /// </summary>
        public int UserId { get; set; }

        public User User { get; set; }
    }
}
