﻿namespace API.Models.Domain
{
    public class Category
    {

        /// <summary>
        /// Model for category
        /// </summary>
        public int ID { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Relationships
        /// </summary>

        public ICollection<Project>  Projects { get; set; }
    }
}
