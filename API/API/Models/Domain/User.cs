﻿namespace API.Models.Domain
{
    public class User
    {
        /// <summary>
        /// Model for User
        /// </summary>
        public int ID { get; set; }
        public string Name { get; set; }
        public string? ImageURL { get; set; }
        public string? Information { get; set; }
        public bool Hidden { get; set; }


        /// <summary>
        /// Relationships
        /// </summary>
        public string UserId { get; set; }
        public ICollection<Skills> Skills { get; set; }
        public ICollection<Project> Projects { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<History> History { get; set; }
        
        public ICollection<Portfolio> Portfolio { get; set; }
    }
}
