﻿namespace API.Models.Domain
{
    public class Link
    {

        /// <summary>
        /// Model for Link
        /// </summary>
        public int Id { get; set; }

        public string Name { get; set; }

        public string URL { get; set; }


        /// <summary>
        /// Relationships
        /// </summary>
        public int ProjectId { get; set; }

        public Project Project { get; set; }
    }
}
