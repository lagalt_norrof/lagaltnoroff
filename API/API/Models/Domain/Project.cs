﻿namespace API.Models.Domain
{
    public class Project
    {

        /// <summary>
        /// Model for Project
        /// </summary>
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string Status { get; set; }
        public int CategoryID { get; set; }
        public int UserId { get; set; }
        /// <summary>
        /// Relationships
        /// </summary>
        public Category Category { get; set; }
      
        public ICollection<User> Users { get; set; }
        public ICollection<Skills> Skills { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<Application> Applications { get; set; }
        public ICollection<Link> Links { get; set; }
    }
}
