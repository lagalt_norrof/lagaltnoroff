﻿using API.Enums;

namespace API.Models.Domain
{
    public class History
    {

        /// <summary>
        /// Model for history
        /// </summary>
        public int Id { get; set; }

        public HistoryType TypeOfHistory { get; set; }


        /// <summary>
        /// Relationships
        /// </summary>
        public int UserId { get; set; }

        public int ProjectId { get; set; }

        public User user { get; set; }
    }
}
