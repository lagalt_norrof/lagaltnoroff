﻿namespace API.Models.DTOs.ApplicationDTOs
{
    public class ApplicationCreateDTO
    {
        /// <summary>
        /// Data transfer object
        /// </summary>
        public int ProjectId { get; set; }
        public int UserId { get; set; }
        public string? Status { get; set; }
        public string? Message { get; set; }

    }
}
