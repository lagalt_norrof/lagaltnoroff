﻿namespace API.Models.DTOs.ApplicationDTOs
{
    public class ApplicationDTO
    {
        /// <summary>
        /// Data transfer object
        /// </summary>
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int UserId { get; set; }

        public string? Message { get; set; }
        public string Status { get; set; }
    }
}
