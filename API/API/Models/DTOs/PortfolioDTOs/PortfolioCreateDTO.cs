﻿namespace API.Models.DTOs.PortfolioDTOs
{
    public class PortfolioCreateDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public string Name { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
    }
}
