﻿namespace API.Models.DTOs.PortfolioDTOs
{
    public class PortfolioDTO
    {
        /// <summary>
        /// Data transfer object
        /// </summary>
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }

    }
}
