﻿using API.Enums;

namespace API.Models.DTOs.HistoryDTOs
{
    public class HistoryDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public int Id { get; set; }

        public HistoryType TypeOfHistory { get; set; }

        public int UserId { get; set; }

        public int ProjectId { get; set; }
    }
}
