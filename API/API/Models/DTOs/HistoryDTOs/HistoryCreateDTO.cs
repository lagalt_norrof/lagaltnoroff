﻿using API.Enums;

namespace API.Models.DTOs.HistoryDTOs
{
    public class HistoryCreateDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public HistoryType TypeOfHistory { get; set; }

        public int UserId { get; set; }

        public int ProjectId { get; set; }
    }
}
