﻿namespace API.Models.DTOs.CategoryDTOs
{
    public class CategoryDTO
    {
        /// <summary>
        /// Data transfer object
        /// </summary>
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
