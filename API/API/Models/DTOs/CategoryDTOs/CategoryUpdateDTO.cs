﻿namespace API.Models.DTOs.CategoryDTOs
{
    public class CategoryUpdateDTO
    {
        /// <summary>
        /// Data transfer object
        /// </summary>
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
