﻿namespace API.Models.DTOs.CategoryDTOs
{
    public class CategoryCreateDTO
    {
        /// <summary>
        /// Data transfer object
        /// </summary>

        public string Name { get; set; }
    }
}
