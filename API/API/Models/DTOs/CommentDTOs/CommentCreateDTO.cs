﻿namespace API.Models.DTOs.CommentDTOs
{
    public class CommentCreateDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public string Message { get; set; }
        public DateTime Date { get; set; }
        public int UserId { get; set; }
        public int ProjectId { get; set; }
    }
}
