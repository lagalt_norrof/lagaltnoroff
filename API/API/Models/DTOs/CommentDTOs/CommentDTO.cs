﻿namespace API.Models.DTOs.CommentDTOs
{
    public class CommentDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public int CommentId { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }
        public int UserId { get; set; }
        public int ProjectId { get; set; }
        public int User { get; set; }
        public int Project { get; set; }
    }
}
