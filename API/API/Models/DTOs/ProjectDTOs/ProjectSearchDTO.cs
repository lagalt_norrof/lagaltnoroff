﻿namespace API.Models.DTOs.ProjectDTOs
{
    public class ProjectSearchDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string Status { get; set; }
        public List<int> Skills { get; set; }
        public List<int> Projects { get; set; }
        //public List<int> Users { get; set; }
    }
}
