﻿using API.Models.DTOs.SkillDTOs;
using API.Models.DTOs.UserDTOs;

namespace API.Models.DTOs.ProjectDTOs
{
    public class ProjectSkillsDTO
    {
        /// <summary>
        /// Data transfer object
        /// </summary>
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string Status { get; set; }
        public ICollection<SkillsDTO> Skills { get; set; }

    }
}
