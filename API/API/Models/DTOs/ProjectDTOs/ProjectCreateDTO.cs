﻿namespace API.Models.DTOs.ProjectDTOs
{
    public class ProjectCreateDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string Status { get; set; }
        public int CategoryID { get; set; }
        public int UserId { get; set; }
    }
}
