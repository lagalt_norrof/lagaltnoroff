﻿namespace API.Models.DTOs.LinkDTOs
{
    public class LinkCreateDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public string Name { get; set; }
        public string URL { get; set; }
        public int ProjectId { get; set; }
    }
}
