﻿namespace API.Models.DTOs.LinkDTOs
{
    public class LinkDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public int Id { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }

    }
}
