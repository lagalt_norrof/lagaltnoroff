﻿namespace API.Models.DTOs.LinkDTOs
{
    public class LinkUpdateDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public int Id { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public int ProjectId { get; set; }
    }
}
