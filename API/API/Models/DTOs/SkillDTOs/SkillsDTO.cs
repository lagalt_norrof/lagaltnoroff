﻿namespace API.Models.DTOs.SkillDTOs
{
    public class SkillsDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public int ID { get; set; }
        public string SkillName { get; set; }
        public List<int> Users { get; set; }
    }
}
