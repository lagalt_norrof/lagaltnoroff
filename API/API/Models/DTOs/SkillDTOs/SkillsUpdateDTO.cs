﻿namespace API.Models.DTOs.SkillDTOs
{
    public class SkillsUpdateDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>

        public int ID { get; set; }
        public string SkillName { get; set; }
    }
}
