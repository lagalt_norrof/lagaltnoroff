﻿namespace API.Models.DTOs.SkillDTOs
{
    public class SkillCreateDTO
    {
        /// <summary>
        /// Data transfer object
        /// </summary>
        public string SkillName { get; set; }
    }
}
