﻿using API.Models.Domain;
using API.Models.DTOs.ProjectDTOs;
using API.Models.DTOs.SkillDTOs;

namespace API.Models.DTOs.UserDTOs
{
    public class UserProjectSkillsDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public int ID { get; set; }
        public string Name { get; set; }
        public string? ImageURL { get; set; }
        public string UserId { get; set; }
        public string? Information { get; set; }
        public ICollection<ProjectDTO> Projects { get; set; }
        public ICollection<SkillsDTO> Skills { get; set; }
    }
}
