﻿namespace API.Models.DTOs.UserDTOs
{
    public class UserSkillsDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public int ID { get; set; }
        public string Name { get; set; }
        public string? ImageURL { get; set; }
        public string? Information { get; set; }
        public List<int> Skills { get; set; }
        public bool Hidden { get; set; }
    }
}
