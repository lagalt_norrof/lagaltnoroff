﻿using API.Models.Domain;

namespace API.Models.DTOs.UserDTOs
{
    public class UserUpdateDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public int ID { get; set; }
        public string Name { get; set; }
        public string? ImageURL { get; set; }
        public string? Information { get; set; }
        public bool Hidden { get; set; }
        public string UserId { get; set; }
        //public List<Skills>? Skills { get; set; }
    }
}
