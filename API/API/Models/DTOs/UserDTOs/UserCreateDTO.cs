﻿using API.Models.Domain;

namespace API.Models.DTOs.UserDTOs
{
    public class UserCreateDTO
    {
        //ID is set automatic right now, if we try to set it manually it throws an error.
        //public int ID { get; set; }
        public string Name { get; set; }
        public string? ImageURL { get; set; }
        public string UserId { get; set; }
        public string? Information { get; set; }
        public bool Hidden { get; set; }

        //Dont think we want to create a user with projects straight away?
        //public ICollection<Skills>? Skills { get; set; }
        //public ICollection<Project>? Projects { get; set; }
    }
}
