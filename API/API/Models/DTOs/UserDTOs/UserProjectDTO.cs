﻿namespace API.Models.DTOs.UserDTOs
{
    public class UserProjectDTO
    {

        /// <summary>
        /// Data transfer object
        /// </summary>
        public int ID { get; set; }
        public string Name { get; set; }
        public string? ImageURL { get; set; }
        public string? Information { get; set; }
        public List<int> Projects { get; set; }
        public bool Hidden { get; set; }
    }
}
