﻿using API.Models.Domain;
using API.Models.DTOs.CategoryDTOs;
using AutoMapper;

namespace API.Mappings.CategoryMappings
{
    public class CategoryMapper : Profile
    {
        /// <summary>
        /// Mapper for category
        /// </summary>
        public CategoryMapper()
        {
            CreateMap<Category, CategoryDTO>().ReverseMap();

            CreateMap<Category, CategoryCreateDTO>().ReverseMap();
            CreateMap<Category, CategoryUpdateDTO>().ReverseMap();
        }


    }
}
