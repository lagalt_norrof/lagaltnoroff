﻿using API.Models.Domain;
using API.Models.DTOs.ApplicationDTOs;
using AutoMapper;

namespace API.Mappings.ApplicationMappings
{
    public class ApplicationMapper : Profile
    {
        /// <summary>
        /// Mapper for profile
        /// </summary>
        public ApplicationMapper()
        {
            CreateMap<Application, ApplicationDTO>().ReverseMap();
            CreateMap<Application, ApplicationCreateDTO>().ReverseMap();
            CreateMap<Application, ApplicationUpdateDTO>().ReverseMap();
        }
    }
}
