﻿using API.Models.Domain;
using API.Models.DTOs.UserDTOs;
using AutoMapper;

namespace API.Mappings.UserMappings
{
    public class UserMappings : Profile
    {
        public UserMappings()
        {
            CreateMap<User, UserDTO>()
                .ReverseMap();

            CreateMap<User, UserProjectSkillsDTO>().ReverseMap();
               // .ForMember(udto => udto.Skills, opt => opt
               // .MapFrom(u => u.Skills.Select(s => s.ID).ToList()))
               // .ForMember(udto => udto.Projects, opt => opt
               //.MapFrom(u => u.Projects.Select(p => p.Id).ToList()))
               // .ReverseMap();

            CreateMap<User, UserSkillsDTO>()
                .ForMember(udto => udto.Skills, opt => opt
                .MapFrom(u => u.Skills.Select(s => s.ID).ToList()))
                .ReverseMap();

            CreateMap<User, UserProjectDTO>()
               .ForMember(udto => udto.Projects, opt => opt
               .MapFrom(u => u.Projects.Select(p => p.Id).ToList()))
               .ReverseMap();

            CreateMap<User, UserCreateDTO>().ReverseMap();

            CreateMap<User, UserUpdateDTO>().ReverseMap();

        }
    }
}
