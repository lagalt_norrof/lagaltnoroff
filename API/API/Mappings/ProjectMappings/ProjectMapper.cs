﻿using API.Models.Domain;
using API.Models.DTOs.ProjectDTOs;
using AutoMapper;

namespace API.Mappings.ProjectMappings
{
    public class ProjectMapper : Profile
    {
        /// <summary>
        /// Mapper for Projects
        /// </summary>
        public ProjectMapper()
        {
            CreateMap<Project, ProjectDTO>().ReverseMap();

            CreateMap<Project, ProjectSearchDTO>().ReverseMap();

            CreateMap<Project, ProjectSkillsDTO>().ReverseMap();
        

            CreateMap<Project, ProjectUserDTO>()
               .ForMember(pdto => pdto.Users, opt => opt
               .MapFrom(p => p.Users.Select(s => s.ID).ToList()))
               .ReverseMap();

            CreateMap<Project, ProjectCommentDTO>()
                .ForMember(pdto => pdto.Comments, opt => opt
                .MapFrom(p => p.Comments.Select(s => s.CommentId).ToList()))
                .ReverseMap();

            CreateMap<Project, ProjectCreateDTO>().ReverseMap();
            CreateMap<Project, ProjectUpdateDTO>().ReverseMap();
        }
    }
}
