﻿using API.Models.Domain;
using API.Models.DTOs.HistoryDTOs;
using AutoMapper;

namespace API.Mappings.HistoryMappings
{
    public class HistoryMapper : Profile
    {
        /// <summary>
        /// Mapper for history
        /// </summary>
        public HistoryMapper()
        {
            CreateMap<History, HistoryDTO>().ReverseMap();
            CreateMap<History, HistoryCreateDTO>().ReverseMap();
        }
    }
}
