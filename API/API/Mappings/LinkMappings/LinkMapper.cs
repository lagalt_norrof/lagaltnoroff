﻿using API.Models.Domain;
using API.Models.DTOs.LinkDTOs;
using AutoMapper;

namespace API.Mappings.LinkMappings
{
    public class LinkMapper : Profile
    {
        /// <summary>
        /// Mapper for link
        /// </summary>
        public LinkMapper()
        {
            CreateMap<Link, LinkDTO>().ReverseMap();
            CreateMap<Link, LinkCreateDTO>().ReverseMap();
            CreateMap<Link, LinkUpdateDTO>().ReverseMap();
        }
    }
}
