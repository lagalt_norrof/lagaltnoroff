﻿using API.Models.Domain;
using API.Models.DTOs.SkillDTOs;
using AutoMapper;

namespace API.Mappings.SkillsMappings
{
    public class SkillsMapper : Profile
    {
        public SkillsMapper()
        {
            CreateMap<Skills, SkillsDTO>().ReverseMap();

            CreateMap<Skills, SkillCreateDTO>().ReverseMap();
            CreateMap<Skills, SkillsUpdateDTO>().ReverseMap();

        }
    }
}
