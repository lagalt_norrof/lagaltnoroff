﻿using API.Models.Domain;
using API.Models.DTOs.CommentDTOs;
using AutoMapper;

namespace API.Mappings.CommentMappings
{
    public class CommentMapper : Profile
    {

        /// <summary>
        /// Mapper for Comment
        /// </summary>
        public CommentMapper()
        {
            CreateMap<Comment, CommentCreateDTO>().ReverseMap();
        }
    }
}
