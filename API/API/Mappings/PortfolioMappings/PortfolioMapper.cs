﻿using API.Models.Domain;
using API.Models.DTOs.PortfolioDTOs;
using AutoMapper;

namespace API.Mappings.PortfolioMappings
{
    public class PortfolioMapper : Profile
    {
        /// <summary>
        /// Mapper for portfolio
        /// </summary>
        public PortfolioMapper()
        {
            CreateMap<Portfolio, PortfolioDTO>().ReverseMap();
            CreateMap<Portfolio, PortfolioCreateDTO>().ReverseMap();
        }
    }
}
