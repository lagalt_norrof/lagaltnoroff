﻿using API.Models.Domain;
using API.Models.DTOs.SkillDTOs;

namespace API.Repositories.Abstract
{
    /// <summary>
    /// Repository interface for Skills
    /// </summary>
    public interface ISkillsRepository
    {
        public Task<IEnumerable<SkillsDTO>> GetAllSkills();
        public Task<SkillsDTO> GetSkillById(int id);

        public Task<SkillCreateDTO> CreateSkill(SkillCreateDTO skill);

        public Task DeleteSkill(int id);
        public Task UpdateSkills(SkillsUpdateDTO skill);

        public bool SkillExists(int id);
    }
}
