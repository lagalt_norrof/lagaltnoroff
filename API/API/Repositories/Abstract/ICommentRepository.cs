﻿using API.Models.DTOs.CommentDTOs;

namespace API.Repositories.Abstract
{

    /// <summary>
    /// Repository Interface for comment
    /// </summary>
    public interface ICommentRepository
    {

        public Task<CommentCreateDTO> CreateComment(CommentCreateDTO createComment);

        public Task DeleteComment(int id);
    }
}
