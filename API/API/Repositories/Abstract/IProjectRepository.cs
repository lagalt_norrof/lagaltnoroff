﻿using API.Models.Domain;
using API.Models.DTOs.CommentDTOs;
using API.Models.DTOs.ProjectDTOs;

namespace API.Repositories.Abstract
{
    /// <summary>
    /// Repository Interface for project
    /// </summary>
    public interface IProjectRepository
    {
        public Task<IEnumerable<ProjectDTO>> GetAllProjects();
        public Task<ProjectDTO> GetProjectByID(int id);
        public Task<IEnumerable<ProjectSearchDTO>> GetProjectByString(string input);

        public Task<ProjectCreateDTO> CreateProject(ProjectCreateDTO projectDTO);

        public Task DeleteProject(int id);
        public Task UpdateProject( ProjectUpdateDTO projectDTO);

        public Task<IEnumerable<ProjectSkillsDTO>> GetSkillsInProjectByID();
        public Task UpdateSkillsInProject(int id, int[] skillsId);

        public Task<IEnumerable<ProjectUserDTO>> GetUsersInProjectById(int id);
        public Task UpdateUsersInProject(int id, int[] userId);

        public Task<IEnumerable<ProjectCommentDTO>> GetCommentInProjectByID(int id);

        public bool ProjectExists(int id);



    }
}
