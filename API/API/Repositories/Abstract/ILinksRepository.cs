﻿using API.Models.DTOs.LinkDTOs;

namespace API.Repositories.Abstract
{
    /// <summary>
    /// Repository Interface for links
    /// </summary>
    public interface ILinksRepository
    {
        public Task<IEnumerable<LinkDTO>> GetAllLinks();

        public Task<LinkDTO> GetLinksById(int id);

        public Task<LinkCreateDTO> CreateLink(LinkCreateDTO linkDTO);

        public Task DeleteLink(int id);

        public Task UpdateLink(LinkUpdateDTO linkDTO);

        public bool LinkExists(int id);
    }
}
