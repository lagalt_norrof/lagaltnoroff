﻿using API.Models.DTOs.PortfolioDTOs;

namespace API.Repositories.Abstract
{    /// <summary>
     /// Repository Interface for portfolio
     /// </summary>
    public interface IPortfolioRepository
    {

        public Task<IEnumerable<PortfolioDTO>> GetAllPortfolios();

        public Task<IEnumerable<PortfolioDTO>> GetUserPortfolio(int id);

        public Task<PortfolioCreateDTO> CreatePortfolio(PortfolioCreateDTO portfolioDTO);

        public Task DeletePortfolio(int id);

        public Task UpdatePortfolio(PortfolioDTO portfolioDTO);

        public bool PortfolioExists (int id);
    }
}
