﻿using API.Models.Domain;
using API.Models.DTOs.ProjectDTOs;
using API.Models.DTOs.UserDTOs;

namespace API.Repositories.Abstract
{
    /// <summary>
    /// Repository Interface for User
    /// </summary>
    public interface IUserRepository
    {
        public Task<IEnumerable<UserDTO>> GetAllUsers();
        public Task<UserDTO> GetUserByID(string userId);
        public Task<IEnumerable<ProjectSkillsDTO>> GetUserWithInfoByID(int Id);
        public Task<UserCreateDTO> CreateUser(UserCreateDTO user);

        public Task DeleteUser(int id);

        public Task UpdateUser(UserUpdateDTO user);

        public Task<IEnumerable<UserProjectDTO>> GetProjectsInUserById(int id);
        public Task<IEnumerable<UserSkillsDTO>> GetSkillsInUserById(int id);
        public Task UpdateSkillsInUser(int id, int[] skillsId);

        public bool userExists(int id);

        public bool userExistsString (string userId);

    }
}
