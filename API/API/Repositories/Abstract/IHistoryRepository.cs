﻿using API.Models.DTOs.HistoryDTOs;

namespace API.Repositories.Abstract
{
    /// <summary>
    /// Repository Interface for history
    /// </summary>
    public interface IHistoryRepository
    {
        public Task<IEnumerable<HistoryDTO>> GetAllHistories();

        public Task<HistoryDTO> GetHistoryById(int id);

        public Task <HistoryCreateDTO> CreateHistory(HistoryCreateDTO history);

        public bool HistoryExists(int id);
    }
}
