﻿using API.Models.DTOs.ApplicationDTOs;

namespace API.Repositories.Abstract
{
    /// <summary>
    /// Repository Interface for application
    /// </summary>
    public interface IApplicationRepository
    {
        public Task<IEnumerable<ApplicationDTO>> GetAllApplications();

        public Task<ApplicationDTO> GetApplicationByID(int id);

        public Task<ApplicationCreateDTO> CreateApplication(ApplicationCreateDTO applicationDTO);

        public Task DeleteApplication(int id);

        public Task UpdateApplication(ApplicationUpdateDTO applicationDTO);

        public bool ApplicationExists(int id);
    }
}
