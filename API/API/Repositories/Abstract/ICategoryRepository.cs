﻿using API.Models.DTOs.CategoryDTOs;

namespace API.Repositories.Abstract
{

    /// <summary>
    /// Repository Interface for category
    /// </summary>
    public interface ICategoryRepository
    {
        public Task<IEnumerable<CategoryDTO>> GetAllCategories();
        public Task<CategoryDTO> GetCategoryById(int id);

        public Task<CategoryCreateDTO> CreateCategory(CategoryCreateDTO category);

        public Task DeleteCategory(int id);

        public Task UpdateCategory(CategoryUpdateDTO category);

        public bool CategoryExists(int id);
    }
}
