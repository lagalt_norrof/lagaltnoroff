﻿using API.Database;
using API.Models.Domain;
using API.Models.DTOs.ProjectDTOs;
using API.Models.DTOs.UserDTOs;
using API.Repositories.Abstract;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace API.Repositories.Concrete
{
    /// <summary>
    /// User Repository, where all the logic happends.
    /// </summary>

    public class UserRepository : IUserRepository
    {
        private readonly LagaltDbContext _context;
        private readonly IMapper _mapper;
        public UserRepository(LagaltDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public async Task<IEnumerable<UserDTO>> GetAllUsers()
        {
            var result = await _context.Users.ToListAsync();
            var mappedResult = _mapper.Map<List<UserDTO>>(result);
            return mappedResult;
        }

        public async Task<UserDTO> GetUserByID(string userId)
        {
            var result = await _context.Users.FirstOrDefaultAsync(u => u.UserId == userId);
            var mappedResult = _mapper.Map<UserDTO>(result);
            return mappedResult;
        }

        public async Task<IEnumerable<ProjectSkillsDTO>> GetUserWithInfoByID(int Id)
        {
            var user = await _context.Users.Include(p => p.Projects)
                .Include(p => p.Projects).ThenInclude(p => p.Skills)
                .Where(u => u.ID == Id).FirstOrDefaultAsync();

            var projects = await _context.Projects.Include(p => p.Skills)
                .Where(p => p.UserId == user.ID).ToListAsync();
         

            foreach (Project project in projects)
            {
                user.Projects.Add(project);
            }
            var mappedResult = _mapper.Map<IEnumerable<ProjectSkillsDTO>>(user.Projects);
            return mappedResult;
        }


        public async Task<UserCreateDTO> CreateUser(UserCreateDTO user)
        {
            bool userExists = await _context.Users.AnyAsync(u => u.UserId == user.UserId);
            if (userExists == true)
                return null;
            var mappedUser = _mapper.Map<User>(user);
            _context.Users.Add(mappedUser);
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task DeleteUser(int id)
        {
            var user = await _context.Users.FindAsync(id);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateUser(UserUpdateDTO updateUser)
        {
            var user = _mapper.Map<User>(updateUser);
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserProjectDTO>> GetProjectsInUserById(int id)
        {
            var user = await _context.Users
               .Include(u => u.Projects)
               .Where(u => u.ID == id)
               .ToListAsync();

            var mappedUser = _mapper.Map<List<UserProjectDTO>>(user);

            return mappedUser;
        }

        public async Task<IEnumerable<UserSkillsDTO>> GetSkillsInUserById(int id)
        {
            var user = await _context.Users
                .Include(u => u.Skills)
                .Where(u => u.ID == id)
                .ToListAsync();

            var mappedUser = _mapper.Map<List<UserSkillsDTO>>(user);

            return mappedUser;
        }

        public async Task UpdateSkillsInUser(int id, int[] skillsId)
        {
            User user = await _context.Users
                .Include(u => u.Skills)
                .Where(s => s.ID == id)
                .FirstAsync();



            List<Skills> skills = new();
            foreach (int skillsIds in skillsId)
            {
                Skills skill = await _context.Skills.FindAsync(skillsIds);
                skills.Add(skill);
            }



            user.Skills = skills;
            await _context.SaveChangesAsync();
        }

        public bool userExists(int id)
        {
            return _context.Users.Any(u => u.ID == id);
        }

        public bool userExistsString(string userId)
        {
            return _context.Users.Any(u => u.UserId == userId);
        }
    }
}
