﻿using API.Database;
using API.Models.Domain;
using API.Models.DTOs.CommentDTOs;
using API.Repositories.Abstract;
using AutoMapper;

namespace API.Repositories.Concrete
{

    /// <summary>
    /// Repository for Comments where the logic happends
    /// </summary>
    public class CommentRepository : ICommentRepository
    {
        private readonly LagaltDbContext _context;
        private readonly IMapper _mapper;
        public CommentRepository(LagaltDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<CommentCreateDTO> CreateComment(CommentCreateDTO createComment)
        {
             var mappedComment = _mapper.Map<Comment>(createComment);
            _context.Add(mappedComment);
            await _context.SaveChangesAsync();

            return createComment;
        }

        public async Task DeleteComment(int id)
        {
            var comment = await _context.Comment.FindAsync(id);
            _context.Comment.Remove(comment);
            await _context.SaveChangesAsync();
        }
    }
}
