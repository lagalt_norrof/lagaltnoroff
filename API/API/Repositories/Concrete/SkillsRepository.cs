﻿using API.Database;
using API.Models.Domain;
using API.Models.DTOs.SkillDTOs;
using API.Repositories.Abstract;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace API.Repositories.Concrete
{

    /// <summary>
    /// Repository for skills where the logic happends.
    /// </summary>
    public class SkillsRepository : ISkillsRepository
    {
        private readonly LagaltDbContext _context;
        private readonly IMapper _mapper;
        public SkillsRepository(LagaltDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<SkillsDTO>> GetAllSkills()
        {
            var skills = await _context.Skills.ToListAsync();
            var mappedSkills = _mapper.Map<List<SkillsDTO>>(skills);
            return mappedSkills;
        }

        public async Task<SkillsDTO> GetSkillById(int id)
        {
            var result = await _context.Skills.FindAsync(id);
            var mappedResult = _mapper.Map<SkillsDTO>(result);
            return mappedResult;
        }

        public async Task<SkillCreateDTO> CreateSkill(SkillCreateDTO skill)
        {
            var mappedSkill = _mapper.Map<Skills>(skill);
            _context.Skills.Add(mappedSkill);
            await _context.SaveChangesAsync();
            return skill;
        }

        public async Task DeleteSkill(int id)
        {
            var skill = await _context.Skills.FindAsync(id);
            _context.Skills.Remove(skill);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateSkills(SkillsUpdateDTO skill)
        {
            var skills = _mapper.Map<Skills>(skill);
            _context.Entry(skills).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool SkillExists(int id)
        {
            return _context.Skills.Any(s => s.ID == id);
        }
    }
}
