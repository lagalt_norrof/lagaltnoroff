﻿using API.Database;
using API.Models.Domain;
using API.Models.DTOs.CommentDTOs;
using API.Models.DTOs.ProjectDTOs;
using API.Models.DTOs.SkillDTOs;
using API.Repositories.Abstract;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace API.Repositories.Concrete
{

    /// <summary>
    /// Repository for project where the logic happends
    /// </summary>
    public class ProjectRepository : IProjectRepository
    {
        private readonly LagaltDbContext _context;
        private readonly IMapper _mapper;

        public ProjectRepository(LagaltDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ProjectCreateDTO> CreateProject(ProjectCreateDTO projectDTO)
        {
            var mappedProject = _mapper.Map<Project>(projectDTO);
            _context.Projects.Add(mappedProject);
            await _context.SaveChangesAsync();
            return projectDTO;
        }

        public async Task<IEnumerable<ProjectDTO>> GetAllProjects()
        {
            var projects = await _context.Projects.ToListAsync();
            var mappedProjects = _mapper.Map<List<ProjectDTO>>(projects);
            return mappedProjects;
        }

        public async Task<ProjectDTO> GetProjectByID(int id)
        {

            var result = await _context.Projects.FindAsync(id);
            var mappedResult = _mapper.Map<ProjectDTO>(result);
            return mappedResult;
        }

        public async Task<IEnumerable<ProjectSearchDTO>> GetProjectByString(string input)
        {
            var result = await _context.Projects.Where(p => p.Name.Contains(input)).ToListAsync();
            var mappedResult = _mapper.Map<List<ProjectSearchDTO>>(result);
            return mappedResult;
        }

        public async Task DeleteProject(int id)
        {
            var project = await _context.Projects.FindAsync(id);
            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateProject(ProjectUpdateDTO projectDTO)
        {
            var project = _mapper.Map<Project>(projectDTO);
            _context.Entry(project).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }


        public async Task UpdateSkillsInProject(int id, int[] skillsId)
        {

            Project project = await _context.Projects
                .Include(S => S.Skills)
                .Where(p => p.Id == id)
                .FirstAsync();



            List<Skills> skills = new();
            foreach (int skillsIds in skillsId)
            {
                Skills skill = await _context.Skills.FindAsync(skillsIds);
                skills.Add(skill);
            }



            project.Skills = skills;
            await _context.SaveChangesAsync();
        }


        public async Task<IEnumerable<ProjectUserDTO>> GetUsersInProjectById(int id)
        {
            var project = await _context.Projects
            .Include(p => p.Users)
            .Where(p => p.Id == id)
            .ToListAsync();

            var mappedProject = _mapper.Map<List<ProjectUserDTO>>(project);

            return mappedProject;
        }

        public async Task UpdateUsersInProject(int id, int[] userId)
        {

            Project project = await _context.Projects
                .Include(S => S.Users)
                .Where(p => p.Id == id)
                .FirstAsync();



            List<User> users = new();
            foreach (int userIds in userId)
            {
                User user = await _context.Users.FindAsync(userIds);
                users.Add(user);
            }



            project.Users = users;
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<ProjectSkillsDTO>> GetSkillsInProjectByID()
        {
            var project = await _context.Projects
                .Include(p => p.Skills)
                .ToListAsync();

            var mappedProject = _mapper.Map<List<ProjectSkillsDTO>>(project);

            return mappedProject;
        }

        public async Task<IEnumerable<ProjectCommentDTO>> GetCommentInProjectByID(int id)
        {
            var comment = await _context.Projects
                .Include(p => p.Comments)
                .Where(p => p.Id == id)
                .ToListAsync();

            var mappedProject = _mapper.Map<List<ProjectCommentDTO>>(comment);

            return mappedProject;
        }

        public bool ProjectExists(int id)
        {
            return _context.Projects.Any(p => p.Id == id);
        }

      
    }
}
