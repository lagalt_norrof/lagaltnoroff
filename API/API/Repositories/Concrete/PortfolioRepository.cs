﻿using API.Database;
using API.Models.Domain;
using API.Models.DTOs.PortfolioDTOs;
using API.Repositories.Abstract;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace API.Repositories.Concrete
{

    /// <summary>
    /// Repository for Portfolio where the logic happends
    /// </summary>
    public class PortfolioRepository : IPortfolioRepository
    {
        private readonly LagaltDbContext _context;
        private readonly IMapper _mapper;


        public PortfolioRepository(LagaltDbContext context, IMapper mapped)
        {
            _context = context;
            _mapper = mapped;
        }

        public async Task<PortfolioCreateDTO> CreatePortfolio(PortfolioCreateDTO portfolioDTO)
        {
            var mappedPortfolio = _mapper.Map<Portfolio>(portfolioDTO);
            _context.Portfolios.Add(mappedPortfolio);
            await _context.SaveChangesAsync();
            return portfolioDTO;
        }

        public async Task DeletePortfolio(int id)
        {
            var portfolio = await _context.Portfolios.FindAsync(id);
            _context.Portfolios.Remove(portfolio);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<PortfolioDTO>> GetAllPortfolios()
        {
            var portfolio = await _context.Portfolios.ToListAsync();
            var mappedPortfolio = _mapper.Map<List<PortfolioDTO>>(portfolio);
            return mappedPortfolio;
        }

        public async Task<IEnumerable<PortfolioDTO>> GetUserPortfolio(int id)
        {
            var portfolio = await _context.Portfolios
                .Include(p => p.User)
                .Where(p => p.UserId == id)
                .ToListAsync();

            var mappedPortfolio = _mapper.Map<List<PortfolioDTO>>(portfolio);

            return mappedPortfolio;
        }

        public bool PortfolioExists (int id)
        {
            return _context.Portfolios.Any(p => p.Id == id);
        }

        public async Task UpdatePortfolio(PortfolioDTO portfolioDTO)
        {
            _context.Entry(_mapper.Map<Portfolio>(portfolioDTO)).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
