﻿using API.Database;
using API.Models.Domain;
using API.Models.DTOs.ApplicationDTOs;
using API.Repositories.Abstract;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace API.Repositories.Concrete
{

    /// <summary>
    /// Repository for Application where the logic happends
    /// </summary>
    public class ApplicationRepository : IApplicationRepository
    {
        private readonly LagaltDbContext _context;
        private readonly IMapper _mapper;

        public ApplicationRepository(LagaltDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public bool ApplicationExists(int id)
        {
           
            
                return _context.Application.Any(p => p.Id == id);
            
        }

        public async Task<ApplicationCreateDTO> CreateApplication(ApplicationCreateDTO applicationDTO)
        {
            Project project = await _context.Projects.Include(u => u.Users).FirstOrDefaultAsync(p => p.Id == applicationDTO.ProjectId);

            User user = await _context.Users.FirstOrDefaultAsync(u => u.ID == applicationDTO.UserId);

            Application app = await _context.Application.Where(p => p.ProjectId == applicationDTO.ProjectId)
                .Where(u => u.UserId == applicationDTO.UserId).FirstOrDefaultAsync();

            var mappedApplication = _mapper.Map<Application>(applicationDTO);

            mappedApplication.Status = "Accepted";

            _context.Application.Add(mappedApplication);

            project.Users.Add(user);

            await _context.SaveChangesAsync();

            return applicationDTO;
        }



        public async Task DeleteApplication(int id)
        {
            var application = await _context.Application.FindAsync(id);
            _context.Application.Remove(application);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<ApplicationDTO>> GetAllApplications()
        {
            var application = await _context.Application.ToListAsync();
            var mappedApplication = _mapper.Map<List<ApplicationDTO>>(application);
            return mappedApplication;
        }

        public async Task<ApplicationDTO> GetApplicationByID(int id)
        {
            var result = await _context.Application.FindAsync(id);
            var mappedResult = _mapper.Map<ApplicationDTO>(result);
            return mappedResult;
        }

        public async Task UpdateApplication(ApplicationUpdateDTO applicationDTO)
        {
            Project project = await _context.Projects.Include(u => u.Users).FirstOrDefaultAsync(p => p.Id == applicationDTO.ProjectId);
            User user = await _context.Users.FirstOrDefaultAsync(u => u.ID == applicationDTO.UserId);

            Application app = await _context.Application.Where(p => p.ProjectId == applicationDTO.ProjectId)
                .Where(u => u.UserId == applicationDTO.UserId).FirstOrDefaultAsync();

            app.Status = applicationDTO.Status;

            if(applicationDTO.Status == "Accepted")
            {
                project.Users.Add(user);
            }

            await _context.SaveChangesAsync();


            //var application = _mapper.Map<Application>(applicationDTO);
            //_context.Entry(application).State = EntityState.Modified;
            //await _context.SaveChangesAsync();
        }


    }
}
