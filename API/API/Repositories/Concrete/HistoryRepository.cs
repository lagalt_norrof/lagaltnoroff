﻿using API.Database;
using API.Models.Domain;
using API.Models.DTOs.HistoryDTOs;
using API.Repositories.Abstract;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace API.Repositories.Concrete
{
    /// <summary>
    /// Repository for history where the logic happends
    /// </summary>
    public class HistoryRepository : IHistoryRepository
    {
        private readonly LagaltDbContext _context;
        private readonly IMapper _mapper;

        public HistoryRepository(LagaltDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<HistoryCreateDTO> CreateHistory(HistoryCreateDTO history)
        {
            var mappedHistory = _mapper.Map<History>(history);
            _context.History.Add(mappedHistory);
            await _context.SaveChangesAsync();
            return history;
        }

        public async Task<IEnumerable<HistoryDTO>> GetAllHistories()
        {
            var history = await _context.History.ToListAsync();
            var mappedHistory = _mapper.Map<List<HistoryDTO>>(history);
            return mappedHistory;
        }

        public async Task<HistoryDTO> GetHistoryById(int id)
        {
            var result = await _context.History.FindAsync(id);
            var mappedResult = _mapper.Map<HistoryDTO>(result);
            return mappedResult;
        }

        public bool HistoryExists(int id)
        {
            return _context.History.Any(p => p.Id == id);
        }
    }
}
