﻿using API.Database;
using API.Models.Domain;
using API.Models.DTOs.LinkDTOs;
using API.Repositories.Abstract;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace API.Repositories.Concrete
{


    /// <summary>
    /// Repository for links where the logic happends
    /// </summary>
    public class LinkRepository : ILinksRepository
    {
        private readonly LagaltDbContext _context;
        private readonly IMapper _mapper;

        public LinkRepository(LagaltDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<LinkDTO>> GetAllLinks()
        {
            var link = await _context.Links.ToListAsync();
            var mappedLinks = _mapper.Map<List<LinkDTO>>(link);
            return mappedLinks;
        }

        public async Task<LinkDTO> GetLinksById(int id)
        {
            var result = await _context.Links.FindAsync(id);
            var mappedResult = _mapper.Map<LinkDTO>(result);
            return mappedResult;
        }

        public async Task<LinkCreateDTO> CreateLink(LinkCreateDTO linkDTO)
        {
            var mappedLink = _mapper.Map<Link>(linkDTO);
            _context.Links.Add(mappedLink);
            await _context.SaveChangesAsync();
            return linkDTO;
        }

        public async Task DeleteLink(int id)
        {
            var link = await _context.Links.FindAsync(id);
            _context.Links.Remove(link);
            await _context.SaveChangesAsync();
        }


        public async Task UpdateLink(LinkUpdateDTO linkDTO)
        {
            var link = _mapper.Map<Link>(linkDTO);
            _context.Entry(link).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool LinkExists(int id)
        {
                return _context.Links.Any(p => p.Id == id);
        }
    }
}
