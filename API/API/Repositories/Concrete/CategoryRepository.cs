﻿using API.Database;
using API.Models.Domain;
using API.Models.DTOs.CategoryDTOs;
using API.Repositories.Abstract;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace API.Repositories.Concrete
{

    /// <summary>
    /// Repository for Category where the logic happends
    /// </summary>
    public class CategoryRepository : ICategoryRepository
    {
        private readonly LagaltDbContext _context;
        private readonly IMapper _mapper;

        public CategoryRepository(LagaltDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<CategoryDTO>> GetAllCategories()
        {
            var category = await _context.Category.ToListAsync();
            var mappedCategories = _mapper.Map<List<CategoryDTO>>(category);
            return mappedCategories;
        }

        public async Task<CategoryDTO> GetCategoryById(int id)
        {
            var result = await _context.Category.FindAsync(id);
            var mappedResult = _mapper.Map<CategoryDTO>(result);
            return mappedResult;
        }

        public async Task<CategoryCreateDTO> CreateCategory(CategoryCreateDTO category)
        {
            var mappedCategory = _mapper.Map<Category>(category);
            _context.Category.Add(mappedCategory);
            await _context.SaveChangesAsync();
            return category;

        }

        public async Task DeleteCategory(int id)
        {
            var category = await _context.Category.FindAsync(id);
            _context.Category.Remove(category);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateCategory(CategoryUpdateDTO category)
        {
            var categors = _mapper.Map<Category>(category);
            _context.Entry(categors).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool CategoryExists(int id)
        {
            return _context.Category.Any(p => p.ID == id);
        }
    }

}
